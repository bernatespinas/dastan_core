use rand::Rng;

use dastan_base::{Biome, GeneratorBundle, Plant, ToItem, time::Season, Size};

use crate::{
	map::{Map, Zone, Tile},
};
use crate::map::building::BuildingGen;
use crate::internet::Internet;

mod planet_generator;
pub use planet_generator::{PlanetGenerator, SeaDepths, LaLaLand};

mod spaceship_generator;
pub use spaceship_generator::{SpaceshipMapGenerator, MySporeCreation1};

mod space_generator;
pub use space_generator::{SpaceGenerator, Space};

use super::MapMode;

pub trait MapGenerator<'a> {
	fn generate(self) -> Result<Map, String>;
}

fn populate_zone_with_biome(
	zone: &mut Zone,
	biome: &Biome,
	gen_bundle: &GeneratorBundle
) {
	// dbg!(format!("zone_height = {}, zone_width = {}", self.universe_info.zone_height, self.universe_info.zone_width));
	for y in zone.iter_rows() {
		for _x in zone.iter_cols() {
			// dbg!(format!("(y, x) = {:?}", (y, _x)));

			let flora_chance = rand::thread_rng().gen_range(0u8..101u8);
			let flora = biome.flora.get_species::<Plant>(
				Season::Winter,
				gen_bundle,
				flora_chance
			);

			zone.push_tile(y, Tile {
				creature: None,
				item: flora.map(|f| f.to_item()),
				terrain_type: biome.terrain,
			});
		}
	}
}

// mod shapes {
// 	#[derive(Clone)]
// 	pub struct Rectangle {
// 		y1: usize,
// 		x1: usize,
// 		y2: usize,
// 		x2: usize,
// 	}

// 	impl Rectangle {
// 		pub fn new(y: usize, x: usize, h: usize, w: usize) -> Rectangle {
// 			Rectangle {y1: y, x1: x, y2: y+h, x2: x+w}
// 		}

// 		pub fn center(&self) -> (usize, usize) {
// 			let center_y = (self.y1 + self.y2) / 2;
// 			let center_x = (self.x1 + self.x2) / 2;
// 			(center_y, center_x)
// 		}

// 		pub fn intersects_with(&self, other: &Rectangle) -> bool {
// 			self.y1 <= other.y2 && self.y2 >= other.y1 && self.x1 <= other.x2 && self.x2 >= other.x1
// 		}
// 	}
// }

pub struct UniverseInfo {
	pub name: String,

	// era: Era or whatever settings I introduce

	pub space_info: MapInfo,
}

impl UniverseInfo {
	pub fn generate_map(
		&self,
		gen_bundle: &GeneratorBundle,
		building_gen: &BuildingGen,
		internet: &mut Internet
	) -> Result<Map, String> {
		Space::new(self, gen_bundle, building_gen, internet).generate()
	}
}

pub struct MapInfo {
	pub name: String,

	pub map_mode: MapMode,

	pub height: u16,
	pub width: u16,

	pub zone_height: u16,
	pub zone_width: u16,
}

impl MapInfo {
	pub fn new(
		name: String,
		map_mode: MapMode,
		size: Size,
		zone_height: u16,
		zone_width: u16
	) -> MapInfo {
		let (height, width) = size_to_map_height_width(size);

		MapInfo {
			name,

			map_mode,

			height,
			width,

			zone_height,
			zone_width,
		}
	}
}

pub fn size_to_map_height_width(size: Size) -> (u16, u16) {
	match size {
		Size::Tiny => (12, 12),
		Size::Small => (24, 24),
		Size::Medium => (48, 48),
		Size::Big => (96, 96),
		Size::Huge => (192, 192),
	}
}