use std::collections::HashMap;
use std::thread;
use std::sync::{Arc, RwLock};

use direction_simple::Direction;

use crate::map::{Map, Zone, ZonePosition, ZoneId, Position};
use crate::file_utils;

use super::ZoneManager;

pub struct ZonedLoopingZoneManager {}

impl ZonedLoopingZoneManager {
	/// Returns the current and surrounding ZoneIds (9 in total), ordered from top-left to bot-right.
	fn current_and_surrounding_zones_ids(
		map: &Map,
		current_zone_id: &ZoneId
	) -> [ZoneId; 9] {
		let curr_y = current_zone_id.0;
		let curr_x = current_zone_id.1;

		[
			(map.get_zone_y_up(curr_y), map.get_zone_x_left(curr_x)),
			(map.get_zone_y_up(curr_y), curr_x),
			(map.get_zone_y_up(curr_y), map.get_zone_x_right(curr_x)),
			(curr_y, map.get_zone_x_left(curr_x)),
			*current_zone_id,
			(curr_y, map.get_zone_x_right(curr_x)),
			(map.get_zone_y_down(curr_y), map.get_zone_x_left(curr_x)),
			(map.get_zone_y_down(curr_y), curr_x),
			(map.get_zone_y_down(curr_y), map.get_zone_x_right(curr_x))
		]
	}

	/// ¿Concurrently? load the Zones that are exactly $radius "distance" from the Zone the player is in/at/_.
	fn load_extra_zones(
		map: &mut Map,
		current_zone_id: &ZoneId,
		radius: u8
	) -> Result<(), String> {
		let extra_zones_ids = Self::radius_zones_ids(
			map,
			current_zone_id,
			radius
		);
		debug!("Found {} extra Zones to load", extra_zones_ids.len());

		for zone_id in extra_zones_ids {
			let extra_zones =
				map.extra_zones.clone()
			;
			let path = map.dir_path.clone();

			thread::spawn(move || {
				// Before loading a Zone, check that it's not already loaded.
				if extra_zones.read().unwrap().contains_key(&zone_id) {
					debug!("Not loading extra Zone {:?} from file, already loaded", zone_id);
				} else {
					debug!("Loading extra Zone {:?} from file", zone_id);

					// TODO Unwrapping Result, I'd like to "?" it.
					let zone = file_utils::load_zone_path(
						&path,
						&zone_id
					).unwrap();

					extra_zones.write().unwrap().insert(
						zone_id,
						zone
					);

					debug!("Loaded extra Zone {:?} from file", zone_id);
				}
			});
		}

		Ok(())
	}

	///
	/// 1 1 1 1 2
	/// 4 . . . 2
	/// 4 . X . 2
	/// 4 . . . 2
	/// 4 3 3 3 3
	/// X: Zone where the player is ¿at/in/_?.
	/// .: Immediatly surrounding Zone, stored in self.zones.
	/// 1-4: Zones loaded in self.extra_zones, in order of insertion.
	fn radius_zones_ids(
		map: &Map,
		current_zone_id: &ZoneId,
		radius: u8
	) -> Vec<ZoneId> {
		let mut zones_ids: Vec<ZoneId> = Vec::with_capacity(
			2 * radius as usize - 1
		);

		debug!("Current zone_id is {:?}", current_zone_id);

		ZonedLoopingZoneManager::add_zones_src_dir_amount(
			map,
			current_zone_id,
			Direction::UpLeft,
			radius,
			Direction::Right,
			&mut zones_ids
		);

		debug!("Added {} Zones after {:?}: {:?}", zones_ids.len(), Direction::Right, zones_ids);

		ZonedLoopingZoneManager::add_zones_src_dir_amount(
			map,
			current_zone_id,
			Direction::UpRight,
			radius,
			Direction::Down,
			&mut zones_ids
		);

		debug!("Added {} Zones after {:?}: {:?}", zones_ids.len(), Direction::Down, zones_ids);

		ZonedLoopingZoneManager::add_zones_src_dir_amount(
			map,
			current_zone_id,
			Direction::DownRight,
			radius,
			Direction::Left,
			&mut zones_ids
		);

		debug!("Added {} Zones after {:?}: {:?}", zones_ids.len(), Direction::Left, zones_ids);

		ZonedLoopingZoneManager::add_zones_src_dir_amount(
			map,
			current_zone_id,
			Direction::DownLeft,
			radius,
			Direction::Up,
			&mut zones_ids
		);

		debug!("Added {} Zones after {:?}: {:?}", zones_ids.len(), Direction::Up, zones_ids);

		debug!("{} Zones before retain", zones_ids.len());

		debug!("self.zones: {:?}", map.zones.keys().collect::<Vec<&ZoneId>>());
		debug!("extra_zones_ids: {:?}", zones_ids);

		zones_ids.retain(|&zone_id| !map.zones.contains_key(&zone_id));

		debug!("{} Zones after retain", zones_ids.len());

		zones_ids
	}

	fn zone_id_apply_dir(
		map: &Map,
		zone_id: &ZoneId,
		dir: Direction
	) -> ZoneId {
		match dir {
			Direction::Up => {(
				map.get_zone_y_up(zone_id.0),
				zone_id.1
			)},
			Direction::Down => {(
				map.get_zone_y_down(zone_id.0),
				zone_id.1
			)},
			Direction::Left => {(
				zone_id.0,
				map.get_zone_x_left(zone_id.1)
			)},
			Direction::Right => {(
				zone_id.0,
				map.get_zone_x_right(zone_id.1)
			)},
			Direction::UpLeft => {(
				map.get_zone_y_up(zone_id.0),
				map.get_zone_x_left(zone_id.1)
			)},
			Direction::UpRight => {(
				map.get_zone_y_up(zone_id.0),
				map.get_zone_x_right(zone_id.1)
			)},
			Direction::DownLeft => {(
				map.get_zone_y_down(zone_id.0),
				map.get_zone_x_left(zone_id.1)
			)},
			Direction::DownRight => {(
				map.get_zone_y_down(zone_id.0),
				map.get_zone_x_right(zone_id.1)
			)}
		}
	}

	fn get_destination_zone_id(
		map: &Map,
		zone_id: &ZoneId,
		dir: Direction,
		amount: u8
	) -> ZoneId {
		let mut destination = zone_id.clone();

		for _ in 0..amount {
			destination = Self::zone_id_apply_dir(map, &destination, dir);
		}

		destination
	}

	fn add_zones_src_dir_amount(
		map: &Map,
		current_zone_id: &ZoneId,
		dir_src: Direction,
		amount: u8,
		dir_dest: Direction,
		zones_ids: &mut Vec<ZoneId>
	) {
		let src = Self::get_destination_zone_id(
			map,
			current_zone_id,
			dir_src,
			amount
		);

		debug!("src ({:?}, {}) is {:?}", dir_src, amount, src);

		for i in 0..amount*2 {
			zones_ids.push(
				Self::get_destination_zone_id(
					map,
					&src,
					dir_dest,
					i
				)
			);
		}
	}
}

impl ZoneManager for ZonedLoopingZoneManager {
	fn load_initial_zones(
		map: &mut Map,
		current_zone_id: &ZoneId
	) -> Result<(), String> {
		let zone_ids =
			ZonedLoopingZoneManager::current_and_surrounding_zones_ids(
				map,
				current_zone_id
			)
		;

		for zone_id in &zone_ids {
			map.add_zone(zone_id)?;
		}

		debug!("{} Zones loaded (current: {:?} + surrounding)", map.zones.len(), current_zone_id);

		ZonedLoopingZoneManager::load_extra_zones(map, current_zone_id, 2)
	}

	fn update_loaded_zones(
		map: &mut Map,
		current_zone_id: &ZoneId
	) -> Result<(), String> {
		// The new current_id and its immediate surrounding zones_ids.
		let new_zone_ids =
			ZonedLoopingZoneManager::current_and_surrounding_zones_ids(
				map,
				current_zone_id
			).to_vec()
		;

		// The HashMap that will contain the new Zones.
		let new_zones: Arc<RwLock<HashMap<ZoneId, Zone>>> =
			Arc::new(RwLock::new(HashMap::new()))
		;

		// The currently loaded Zones. I extract them so I can move them.
		let mut old_zones =
			std::mem::take(&mut map.zones)
		;

		let mut handles = Vec::new();

		// Fetch the "new" set of Zones, from map.zones if present or from their serialized files otherwise.
		for zone_id in new_zone_ids {
			let zone = old_zones.remove(&zone_id);
			let dir_path = map.dir_path.clone();

			let new_zones_clone = new_zones.clone();
			let extra_zones = map.extra_zones.clone();

			let handle = thread::spawn(move || {
				debug!("Loading Zone {:?}", zone_id);

				match zone {
					Some(zone) => {
						// Zone was already loaded in map.zones.
						new_zones_clone.write().unwrap().insert(zone_id, zone);
						debug!("Loaded Zone {:?} from old_zones", zone_id);
					},
					None => {
						// Zone was not already loaded in map.zones.
						match extra_zones.write().unwrap().remove(&zone_id) {
							Some(extra_zone) => {
								// Zones was already loaded in map.extra_zones.
								new_zones_clone.write().unwrap().insert(
									zone_id,
									extra_zone
								);
								debug!("Loaded Zone {:?} from extra_zones", zone_id);
							},
							None => {
								// Zone was not already loaded in map.extra_zones.
								// Deserialize its file and insert it.
								let zone_des = file_utils::load_zone_path(
									&dir_path,
									&zone_id
								).unwrap();

								new_zones_clone.write().unwrap().insert(
									zone_id,
									zone_des
								);
								debug!("Loaded Zone {:?} from unloaded file", zone_id);
							}
						}
					}
				};
			});

			handles.push(handle);
		}

		// TODO I think I don't know when to use join.
		for handle in handles {
			handle.join().unwrap();
		}

		// Store all unneeded Zones in extra_zones.
		// TODO Save them or keep them around compressed.
		for (zone_id, zone) in old_zones {
			map.extra_zones.write().unwrap().insert(zone_id, zone);
		}

		ZonedLoopingZoneManager::load_extra_zones(map, current_zone_id, 2)?;

		map.zones = std::mem::take(&mut new_zones.write().unwrap());

		Ok(())
	}

	fn pos_apply_dir(
		map: &Map,
		zone_pos: &crate::map::ZonePosition,
		dir: Direction
	) -> Result<ZonePosition, ()> {
		// Since the Map is expected to be looping, this is always Ok(..).
		Ok(match dir {
			Direction::Up => {
				if zone_pos.pos.y > 0 {
					ZonePosition {
						zone_id: zone_pos.zone_id,
						pos: Position::new(zone_pos.pos.y-1, zone_pos.pos.x)
					}
				} else {
					ZonePosition {
						zone_id: (
							map.get_zone_y_up(zone_pos.zone_id.0),
							zone_pos.zone_id.1
						),
						pos: Position::new(map.zone_height - 1, zone_pos.pos.x),
					}
				}
			},

			Direction::Down => {
				if zone_pos.pos.y < map.zone_height - 1 {
					ZonePosition {
						zone_id: zone_pos.zone_id,
						pos: Position::new(zone_pos.pos.y + 1, zone_pos.pos.x),
					}
				} else {
					ZonePosition {
						zone_id: (
							map.get_zone_y_down(zone_pos.zone_id.0),
							zone_pos.zone_id.1
						),
						pos: Position::new(0, zone_pos.pos.x),
					}
				}
			},

			Direction::Right => {
				if zone_pos.pos.x < map.zone_width - 1 {
					ZonePosition {
						zone_id: zone_pos.zone_id,
						pos: Position::new(zone_pos.pos.y, zone_pos.pos.x + 1),
					}
				} else {
					ZonePosition {
						zone_id: (
							zone_pos.zone_id.0,
							map.get_zone_x_right(zone_pos.zone_id.1)
						),
						pos: Position::new(zone_pos.pos.y, 0),
					}
				}
			},

			Direction::Left => {
				if zone_pos.pos.x > 0 {
					ZonePosition {
						zone_id: zone_pos.zone_id,
						pos: Position::new(zone_pos.pos.y, zone_pos.pos.x - 1),
					}
				} else {
					ZonePosition {
						zone_id: (
							zone_pos.zone_id.0,
							map.get_zone_x_left(zone_pos.zone_id.1)
						),
						pos: Position::new(zone_pos.pos.y, map.zone_width - 1),
					}
				}
			},

			Direction::UpRight => {
				let (y, zone_id_y) =
					if zone_pos.pos.y > 0 {
						(
							zone_pos.pos.y - 1,
							zone_pos.zone_id.0
						)
					} else {
						(
							map.zone_height - 1,
							map.get_zone_y_up(zone_pos.zone_id.0)
						)
					}
				;
				let (x, zone_id_x) =
					if zone_pos.pos.x < map.zone_width - 1 {
						(
							zone_pos.pos.x + 1,
							zone_pos.zone_id.1
						)
					} else {
						(
							0,
							map.get_zone_x_right(zone_pos.zone_id.1)
						)
					}
				;

				ZonePosition {
					zone_id: (zone_id_y, zone_id_x),
					pos: Position::new(y, x)
				}
			},

			Direction::UpLeft => {
				let (y, zone_id_y) =
					if zone_pos.pos.y > 0 {
						(
							zone_pos.pos.y - 1,
							zone_pos.zone_id.0
						)
					} else {
						(
							map.zone_height - 1,
							map.get_zone_y_up(zone_pos.zone_id.0)
						)
					}
				;
				let (x, zone_id_x) =
					if zone_pos.pos.x > 0 {
						(
							zone_pos.pos.x - 1,
							zone_pos.zone_id.1
						)
					} else {
						(
							map.zone_width - 1,
							map.get_zone_x_left(zone_pos.zone_id.1)
						)
					}
				;

				ZonePosition {
					zone_id: (zone_id_y, zone_id_x),
					pos: Position::new(y, x)
				}
			},

			Direction::DownRight => {
				let (y, zone_id_y) =
					if zone_pos.pos.y < map.zone_height - 1 {
						(
							zone_pos.pos.y + 1,
							zone_pos.zone_id.0
						)
					} else {
						(
							0,
							map.get_zone_y_down(zone_pos.zone_id.0)
						)
					}
				;
				let (x, zone_id_x) =
					if zone_pos.pos.x < map.zone_width - 1 {
						(
							zone_pos.pos.x + 1,
							zone_pos.zone_id.1
						)
					} else {
						(
							0,
							map.get_zone_x_right(zone_pos.zone_id.1)
						)
					}
				;

				ZonePosition {
					zone_id: (zone_id_y, zone_id_x),
					pos: Position::new(y, x)
				}
			},

			Direction::DownLeft => {
				let (y, zone_id_y) =
					if zone_pos.pos.y < map.zone_height - 1 {
						(
							zone_pos.pos.y + 1,
							zone_pos.zone_id.0
						)
					} else {
						(
							0,
							map.get_zone_y_down(zone_pos.zone_id.0)
						)
					}
				;
				let (x, zone_id_x) =
					if zone_pos.pos.x > 0 {
						(
							zone_pos.pos.x - 1,
							zone_pos.zone_id.1
						)
					} else {
						(
							map.zone_width - 1,
							map.get_zone_x_left(zone_pos.zone_id.1)
						)
					}
				;

				ZonePosition {
					zone_id: (zone_id_y, zone_id_x),
					pos: Position::new(y, x)
				}
			},
		})
	}

	fn pos_apply_dir_amount(
		map: &Map,
		zone_pos: &crate::map::ZonePosition,
		dir: Direction,
		amount: usize
	) -> Result<ZonePosition, ZonePosition> {
		let mut final_zone_pos = *zone_pos;

		for _i in 0..amount {
			final_zone_pos = Self::pos_apply_dir(
				map,
				&final_zone_pos,
				dir
			).unwrap();
		}

		Ok(final_zone_pos)
	}
}