use std::collections::HashMap;

use crate::ai::CreatureAi;

use super::{Tile, Position, TerrainType};

#[derive(Serialize, Deserialize)]
pub struct Zone {
	grid: Vec<Vec<Tile>>,
	// biome: Biome,

	creature_ais: HashMap<Position, CreatureAi>,
	// item_ais: HashMap<Position, ItemAi>,
	covered_terrains: HashMap<Position, TerrainType>,
}

impl Zone {
	pub fn with_capacity(height: u16, width: u16) -> Zone {
		let mut grid = Vec::with_capacity(height as usize);
		for _ in 0..height {
			grid.push(Vec::with_capacity(width as usize));
		}

		Zone {
			grid,

			creature_ais: HashMap::new(),
			covered_terrains: HashMap::new(),
		}
	}

	pub fn tile_yx(&self, y: usize, x: usize) -> &Tile {
		&self.grid[y][x]
	}

	pub fn tile(&self, pos: &Position) -> &Tile {
		&self.grid[pos.y as usize][pos.x as usize]
	}

	pub fn tile_mut_yx(&mut self, y: usize, x: usize) -> &mut Tile {
		&mut self.grid[y][x]
	}

	pub fn tile_mut(&mut self, pos: &Position) -> &mut Tile {
		&mut self.grid[pos.y as usize][pos.x as usize]
	}

	pub fn iter_rows(&self) -> std::ops::Range<usize> {
		0..self.grid.capacity()
	}

	pub fn iter_rows_begin_at(&self, row: usize) -> std::ops::Range<usize> {
		row..self.grid.capacity()
	}

	pub fn iter_cols(&self) -> std::ops::Range<usize> {
		0..self.grid[0].capacity()
	}

	pub fn iter_cols_begin_at(&self, col: usize) -> std::ops::Range<usize> {
		col..self.grid[0].capacity()
	}

	pub fn push_tile(&mut self, y: usize, tile: Tile) {
		self.grid[y].push(tile)
	}

	pub fn creature_ais_take(&mut self) -> HashMap<Position, CreatureAi> {
		std::mem::take(&mut self.creature_ais)
	}

	pub fn creature_ais_set(&mut self, ais: HashMap<Position, CreatureAi>) {
		self.creature_ais = ais;
	}

	pub fn creature_ais_add(&mut self, pos: Position, creature_ai: CreatureAi) {
		self.creature_ais.insert(pos, creature_ai);
	}

	pub fn terrain_cover(
		&mut self,
		pos: Position,
		new_terrain_type: TerrainType
	) {
		debug_assert!(
			!self.terrain_covered_there(&pos),
			"terrain_put_over but terrain_covered.is_some()..."
		);

		let old_terrain_type = self.tile(&pos).terrain_type;
		self.covered_terrains.insert(pos, old_terrain_type);
		
		self.tile_mut(&pos).terrain_put(new_terrain_type);
	}

	pub fn terrain_covered_there(&self, pos: &Position) -> bool {
		self.covered_terrains.contains_key(pos)
	}

	pub fn terrain_uncover(
		&mut self,
		pos: &Position
	) {
		if let Some(terrain_type) = self.covered_terrains.remove(pos) {
			self.tile_mut(pos).terrain_put(terrain_type);
		} else {
			// TODO Ignore?
			panic!("No terrain to uncover!! Position: {:?}", pos);
		}
	}

	pub fn cost_matrix(&self) -> Vec<Vec<u8>> {
		let mut cost_matrix = Vec::with_capacity(self.grid.len());

		for row in &self.grid {
			let mut cost_row: Vec<u8> = Vec::with_capacity(row.len());

			for tile in row {
				cost_row.push(tile.cost());
			}

			cost_matrix.push(cost_row);
		}

		cost_matrix
	}
}