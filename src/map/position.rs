use std::fmt;

pub type ZoneId = (u16, u16);

#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Serialize, Deserialize)]
pub struct Position {
	pub y: u16,
	pub x: u16,
}

impl Position {
	pub fn new(y: u16, x: u16) -> Position {
		Position {
			y,
			x,
		}
	}

	pub fn from(pos: &Position, dy: i16, dx: i16) -> Position {
		let y = 
			if dy >= 0 {
				pos.y + dy as u16
			}
			else {
				pos.y - (-dy) as u16
			}
		;

		let x = 
			if dx >= 0 {
				pos.x + dx as u16
			}
			else {
				pos.x - dx as u16
			}
		;
		Position {
			y,
			x,
		}
	}

	pub fn update(&mut self, y: u16, x: u16) {
		self.y = y;
		self.x = x;
	}

	// TODO Use this when pathfinding...
	// fn distance_to(&self, other: &Position) -> u32 {
	// 	let dy = (self.y - other.y) as f64;
	// 	let dx = (self.x - other.x) as f64;
	// 	((dy*dy + dx*dx) as f64).sqrt() as u32
	// }

	// pub fn is_adjacent_to(&self, other: &Position) -> bool {
	// 	other.y <= self.y+1 &&
	// 	other.y >= self.y-1 &&
	// 	other.x <= self.x+1 &&
	// 	other.x >= self.x-1
	// }

	// TODO An alternative version that avoids doing substractions when that would lead to ¿overflows?. Does it work correctly?
	// pub fn is_adjacent_to(&self, other: &Position) -> bool {
	// 	other.y <= self.y+1 &&
	// 		if self.y == 0 {
	// 			other.y == self.y
	// 		}
	// 		else {
	// 			other.y >= self.y-1
	// 		}
	// 	&& other.x <= self.x+1 &&
	// 		if self.x == 0 {
	// 			other.x == self.x
	// 		}
	// 		else {
	// 			other.x >= self.x-1
	// 		}
	// }
}

impl fmt::Display for Position {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "({}, {})", self.y, self.x)
	}
}

impl From<Position> for (u16, u16) {
	fn from(pos: Position) -> (u16, u16) {
		(pos.x, pos.y)
	}
}

impl From<(u16, u16)> for Position {
	fn from((y, x): (u16, u16)) -> Position {
		Position {
			y,
			x
		}
	}
}

// impl std::ops::Deref for Position {
// 	type Target = (usize, usize);

// 	fn deref(&self) -> &(usize, usize) {
// 		&(self.y, self.x)
// 	}
// }

// impl From<&Position> for (u16, u16) {
// 	fn from(pos: &Position) -> (u16, u16) {
// 		(pos.x, pos.y)
// 	}
// }

impl From<&Position> for (usize, usize) {
	fn from(pos: &Position) -> (usize, usize) {
		(pos.x as usize, pos.y as usize)
	}
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct ZonePosition {
	pub pos: Position,
	pub zone_id: ZoneId,
}

impl ZonePosition {
	pub fn new(zone_id: ZoneId, pos: Position) -> ZonePosition {
		ZonePosition {
			zone_id,
			pos,
		}
	}

	pub fn same_zone(&self, other: &ZonePosition) -> bool {
		self.zone_id == other.zone_id
	}
}

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct MapZonePos {
	pub map_file_path: String,
	pub zone_pos: ZonePosition,
}