use dastan_base::{Creature, ToCreature, Item, ToItem, Furniture, Terrain, TerrainBundle, terrain::{TerrainType, TerrainTypeGenerator}};

/// In each tile of the map...:
///  · There is a terrain (floor, wall, water...).
///    · Terrains might or might not block passage.
///    · Since there probably is the same Terrain in multiple Tiles, I store a TerrainType instead, which is smaller, to represent each actual Terrain.
///  · There might be an item, but only one (a potion, a sword...).
///    · Items might or might not block passage.
///  · There might be a creature, but only one.
///    · Creatures always block passage.
#[derive(Serialize, Deserialize)]
pub struct Tile {
	pub creature: Option<Creature>,
	pub item: Option<Item>,
	pub terrain_type: TerrainType,
}

impl Tile {
	pub fn terrain<'a>(
		&self,
		terrain_bundle: &'a TerrainBundle
	) -> &'a Terrain {
		match self.terrain_type {
			TerrainType::Foundation(foundation_type) => terrain_bundle.foundation_gen.terrain(&foundation_type),
			TerrainType::Liquid(liquid_type) => terrain_bundle.liquid_gen.terrain(&liquid_type),
		}
	}

	// pub fn terrain_mut(&mut self, map: &Map) -> &mut Terrain {
	// 	&mut self.terrain_name
	// }

	pub fn terrain_put(&mut self, terrain_type: TerrainType) {
		self.terrain_type = terrain_type;
	}

	pub fn item(&self) -> &Option<Item> {
		&self.item
	}

	pub fn item_mut(&mut self) -> &mut Option<Item> {
		&mut self.item
	}

	pub fn item_take(&mut self) -> Option<Item> {
		self.item.take()
	}

	pub fn item_put<T>(&mut self, item: T)
	where
		T: ToItem,
	{
		debug_assert!(
			self.item.is_none(),
			"item_put -> self.item.is_some()!!"
		);

		self.item = Some(item.to_item());
	}

	pub fn item_there(&self) -> bool {
		self.item.is_some()
	}

	pub fn creature(&self) -> &Option<Creature> {
		&self.creature
	}

	pub fn creature_mut(&mut self) -> &mut Option<Creature> {
		&mut self.creature
	}

	pub fn creature_take(&mut self) -> Option<Creature> {
		self.creature.take()
	}

	pub fn creature_put<T>(&mut self, creature: T)
	where
		T: ToCreature,
	{
		if let Some(cretin) = &self.creature {
			debug!("creature_put in tile with {} already there, panic now", cretin.name());
		}
		debug_assert!(
			self.creature.is_none(),
			"creature_put -> self.creature.is_some()!!"
		);
		self.creature = Some(creature.to_creature());
	} 

	pub fn creature_there(&self) -> bool {
		self.creature.is_some()
	}

	pub fn blocks_passage(&self) -> bool {
		self.terrain_type.blocks_passage()
		|| self.creature.is_some()
		|| self.item_blocks_passage()
	}

	pub fn blocks_passage_terrain_item(&self) -> bool {
		let terrain_blocks_passage = self
			.terrain_type
			.blocks_passage()
		;

		let item_blocks_passage = match &self.item {
			Some(item) => item.blocks_passage(),
			None => false,
		};

		terrain_blocks_passage || item_blocks_passage
	}

	pub fn item_blocks_passage(&self) -> bool {
		if let Some(item) = &self.item {
			item.blocks_passage()
		} else {
			false
		}
	}

	pub fn blocks_creature_item(&self) -> bool {
		self.creature.is_some() || self.item_blocks_passage()
	}

	pub fn can_walk(&self) -> bool {
		self.terrain_type.is_floor() && !self.blocks_creature_item()
	}

	pub fn can_swim(&self) -> bool {
		self.terrain_type.is_liquid() && !self.blocks_creature_item()
	}

	pub fn cost(&self) -> u8 {
		if let Some(Item::Furniture(box Furniture::Passage(_))) = &self.item {
			2
		} else if self.blocks_passage_terrain_item() {
			0
		} else {
			1
		}
	}

	pub fn unlockable_passage(&self) -> bool {
		match &self.item {
			Some(Item::Furniture(box Furniture::Passage(passage))) => passage.blocks_passage,
			_ => false,
		}
	}
}