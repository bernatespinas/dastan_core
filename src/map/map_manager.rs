use std::collections::HashMap;

use super::file_utils;

use super::{Map, ZoneId};

pub struct MapManager {
	maps: HashMap<String, Map>,
}

impl Default for MapManager {
	fn default() -> MapManager {
		MapManager {
			maps: HashMap::with_capacity(2),
		}
	}
}

impl MapManager {
	pub fn load_map(&mut self, map_file_path: &str) -> Result<(), String> {
		debug!("loading map at map_dir {}", map_file_path);

		if self.maps.contains_key(map_file_path) {
			debug!("map at map_dir {} already loaded", map_file_path);
		} else {
			let map_file = file_utils::read(map_file_path, "map")?;
			let map: Map = file_utils::deserialize(&map_file, "map")?;
	
			self.maps.insert(map_file_path.to_string(), map);

			debug!("map at map_dir {} loaded from file", map_file_path);
		}

		Ok(())
	}

	/// Load a Map and the required Zones around a specific Zone.
	pub fn load_map_and_required_zones(
		&mut self,
		map_file_path: &str,
		zone_id: &ZoneId
	) -> Result<(), String> {
		self.load_map(map_file_path)?;
		debug!("load_map done");

		if self.map_zones_not_loaded(self.map(map_file_path)) {
			// If the Map doesn't have any Zones loaded, it might (?) mean it's the first time it's being loaded and I need to load its initial Zones.
			debug!("calling load_initial_zones, not update_loaded_zones");
			self.map_mut(map_file_path).load_initial_zones(zone_id)
		} else {
			// If the Map and its Zones had already been loaded, make sure the Zones the player needs are the current ones.
			debug!("calling update_loaded_zones, not load_initial_zones");
			self.map_mut(map_file_path).update_loaded_zones(zone_id)
		}
	}

	pub fn insert_map(&mut self, map: Map) {
		debug!("inserting map at map_dir {}", map.dir_path);

		self.maps.insert(map.dir_path.to_string(), map);
	}

	pub fn save_maps(&self) -> Result<(), String> {
		for map in self.maps.values() {
			map.save_zones()?;
		}

		Ok(())
	}

	pub fn map(&self, map_file_path: &str) -> &Map {
		self.maps.get(map_file_path).unwrap()
	}

	pub fn map_mut(&mut self, map_file_path: &str) -> &mut Map {
		self.maps.get_mut(map_file_path).unwrap()
	}

	fn map_zones_not_loaded(&self, map: &Map) -> bool {
		map.zones.is_empty()
	}
}