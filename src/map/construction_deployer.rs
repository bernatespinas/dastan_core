use direction_simple::Direction;

use dastan_base::GeneratorBundle;

use crate::ai::Ai;

use super::{Map, ZonePosition};
use super::building::ConstructionLayout;

pub fn deploy(
	map: &mut Map,
	top_left: &ZonePosition,
	construction_layout: &ConstructionLayout,
	gen_bundle: &GeneratorBundle
) -> Result<(), String> {
	let mut zone_pos = *top_left;

	for row in &construction_layout.matrix {
		for icon in row {
			// TODO I remove any Item that could be in that zone_pos so that I can place whatever is specified in construction_layout.
			map.zone_tile_mut(&zone_pos).item_take();

			let construction_tile = construction_layout
				.mapping
				.get(&icon)
				.unwrap()
			;

			map.terrain_cover(&zone_pos, construction_tile.terrain_type);

			if let Some(item_code) = construction_tile.item {
				let item = gen_bundle.gen_item(item_code);
				map.zone_tile_mut(&zone_pos).item_put(item);
			}

			if let Some((creature_code, ai_info)) = &construction_tile.creature {
				let creature = gen_bundle.gen_creature(creature_code);
				map.zone_tile_mut(&zone_pos).creature_put(creature);

				let creature_ai: Ai = ai_info.into();
				map.creature_ais_add(zone_pos.clone(), creature_ai);
			}

			zone_pos = map.pos_apply_dir(
				&zone_pos,
				Direction::Right
			).unwrap();
		}

		zone_pos = map.pos_apply_dir(&zone_pos, Direction::Down).unwrap();
		zone_pos.pos.x = top_left.pos.x;
	}

	Ok(())
}

// pub fn deploy_building_transform(
// 	map: &mut Map,
// 	top_left: &ZonePosition,
// 	construction_layout: &ConstructionLayout,
// 	rotate: bool,
// 	v_flip: bool,
// 	h_flip: bool,
// 	gen_bundle: &GeneratorBundle
// ) -> Result<(), String> {
// 	// debug!("deploying building with orientation {:?}", orientation);
// 	debug!("deploying building with rotate {}, v_flip {} and h_flip {}", rotate, v_flip, h_flip);

// 	let mut zone_pos = *top_left;

// 	// let (i_range, j_range) = if rotate {
// 	// 	(x_range, y_range)
// 	// } else {
// 	// 	(y_range, x_range)
// 	// };

// 	let (v_range, h_range)
// 		: (Box<dyn Iterator<Item=usize>>, Box<dyn Iterator<Item = usize>>)
// 	= 
// 		if v_flip {
// 			let v_range = (0..construction_layout.height()).rev();

// 			if h_flip {
// 				let h_range = (0..construction_layout.width()).rev();

// 				(box v_range, box h_range)
// 			} else {
// 				let h_range = 0..construction_layout.width();

// 				(box v_range, box h_range)
// 			}
// 		} else {
// 			let v_range = 0..construction_layout.height();

// 			if h_flip {
// 				let h_range = (0..construction_layout.width()).rev();

// 				(box v_range, box h_range)
// 			} else {
// 				let h_range = 0..construction_layout.width();

// 				(box v_range, box h_range)
// 			}
// 		}
// 	;

// 	let (i_range, j_range) = if rotate {
// 		(h_range, v_range)
// 	} else {
// 		(v_range, h_range)
// 	};

// 	for i in i_range {
// 		for j in j_range {
// 			let icon =
// 				if rotate {
// 					construction_layout.matrix
// 						.get(j)
// 						.unwrap()
// 						.get(i)
// 						.unwrap()
// 				} else {
// 					construction_layout.matrix
// 						.get(i)
// 						.unwrap()
// 						.get(j)
// 						.unwrap()
// 				}
// 			;

// 			BuildingDeployer::deploy_tile(
// 				map,
// 				&zone_pos,
// 				construction_layout,
// 				icon,
// 				gen_bundle
// 			);

// 			zone_pos = map.pos_apply_dir(
// 				&zone_pos,
// 				Direction::Right
// 			).unwrap();
// 		}

// 		zone_pos = map.pos_apply_dir(&zone_pos, Direction::Down).unwrap();
// 		zone_pos.pos.x = top_left.pos.x;
// 	}

// 	Ok(())
// }

// fn deploy_tile(
// 	map: &mut Map,
// 	zone_pos: &ZonePosition,
// 	construction_layout: &ConstructionLayout,
// 	icon: &char,
// 	gen_bundle: &GeneratorBundle
// ) {
// 	// TODO I remove any Item that could be in that zone_pos so that I can place whatever is specified in construction_layout.
// 	map.zone_tile_mut(&zone_pos).item_take();

// 	match construction_layout.mapping.get(&icon).unwrap() {
// 		ConstructionType::Terrain(terrain) => {
// 			// let terrain = gen_bundle.gen_terrain(code);
// 			// let terrain = self.terrain(code);
// 			let terrain_type = gen_bundle.terrain_bundle.name_to_type(terrain);
// 			map.terrain_cover(&zone_pos, terrain_type);
// 		},
// 		ConstructionType::Item {item, terrain} => {
// 			let item = gen_bundle.gen_item(item);
// 			map.zone_tile_mut(&zone_pos).item_put(item);

// 			// let terrain = gen_bundle.gen_terrain(terrain_code);
// 			// let terrain = self.terrain(terrain_code);
// 			let terrain_type = gen_bundle.terrain_bundle.name_to_type(terrain);
// 			map.terrain_cover(&zone_pos, terrain_type);
// 		},
// 	};
// }