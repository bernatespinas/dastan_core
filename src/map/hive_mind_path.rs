use std::collections::HashMap;

use dastan_base::creature::humanoid::Humanoid;

use super::ZonePosition;

#[derive(Eq, Hash)]
pub enum Service {
	Food,
	House(String),
}

impl PartialEq for Service {
	fn eq(&self, other: &Self) -> bool {
		match self {
			Service::Food => match other {
				Service::Food => true,
				_ => false,
			},
			Service::House(self_house) => match other {
				Service::House(other_house) => self_house.eq(other_house),
				_ => false,
			},
		}
	}
}

pub struct HiveMindPath {
	top_left: ZonePosition,
	bot_right: ZonePosition,
	// height: u16,
	// width: u16,

	services: HashMap<Service, ZonePosition>,
	humanoids: Vec<Humanoid>,
}

// impl Default for HiveMindPath {
//	 fn default() -> Self {
//		 HiveMindPath {
//			 services: HashMap::new(),
//			 humanoids: Vec::new(),
//		 }
//	 }
// }

impl HiveMindPath {
	pub fn new(
		top_left: ZonePosition,
		bot_right: ZonePosition,
		// height: u16,
		// width: u16
	) -> HiveMindPath {
		HiveMindPath {
			top_left,
			bot_right,

			services: HashMap::new(),
			humanoids: Vec::new(),
		}
	}
	
	fn add_service(&mut self, service: Service, zone_pos: ZonePosition) {
		self.services.insert(service, zone_pos);
	}

	// fn remove_service(&mut self, service: &Service,)

	fn find_service(&self, service: &Service) -> Option<&ZonePosition> {
		self.services.get(service)
	}

	fn assimilate_humanoid(&mut self, humanoid: Humanoid) {
		self.humanoids.push(humanoid);
	}
}