use std::collections::HashMap;

use dastan_base::{Generator, Generate};
use dastan_base::terrain::{FoundationMaterial, FoundationType, TerrainType};

use crate::ai::{AiPriority, AiTask, ChaseCreature};

// mod small_house;

type ConstructionMapping<'a> = HashMap<char, ConstructionTile<'a>>;

// /// Some styles that may help make buildings feel more ¿adapted?
// /// to their "environment" and add some variation.
// enum Style {
//	 Dark,
//	 Light,
//	 Medieval,
//	 Modern,
//	 Rustic,
// }

#[derive(Clone)]
pub struct ConstructionTile<'a> {
	pub terrain_type: TerrainType,
	pub item: Option<&'a str>,
	pub creature: Option<(&'a str, Vec<(AiPriority, AiTask)>)>,
}

impl<'a> ConstructionTile<'a> {
	fn new(terrain_type: TerrainType) -> ConstructionTile<'a> {
		ConstructionTile {
			terrain_type,
			item: None,
			creature: None,
		}
	}

	fn with_item(mut self, item: &'a str) -> ConstructionTile<'a> {
		self.item = Some(item);

		self
	}

	fn with_creature<T>(
		mut self,
		creature: &'a str,
		ai_info: Vec<(AiPriority, T)>,
	) -> ConstructionTile<'a>
	where
		AiTask: From<T>,
	{
		let ai_info_mapped: Vec<(AiPriority, AiTask)> = ai_info
			.into_iter()
			.map(|(ai_priority, ai_task)| (ai_priority, ai_task.into()))
			.collect()
		;
		self.creature = Some((creature, ai_info_mapped));

		self
	}
}

/// A struct that contains a "semi-visual" representation of a room
/// using characters chosen by the programmer/developer and
/// a HashMap that maps these to actual objects.
#[derive(Clone)]
pub struct ConstructionLayout<'a> {
	pub matrix: Vec<Vec<char>>,
	pub mapping: ConstructionMapping<'a>,
}

impl<'a> ConstructionLayout<'a> {
	/// Creation fails when a character that hasn't been defined is used.
	/// Spaces are ignored because they can help align characters.
	fn new(
		mut layout: String,
		mapping: ConstructionMapping<'a>
	) -> Result<Self, String> {
		layout.retain(|x| x != ' ' && x != '\t');

		let mut matrix: Vec<Vec<char>> = Vec::with_capacity(
			layout.lines().count()
		);

		for line in layout.lines() {
			let mut row: Vec<char> = Vec::with_capacity(line.len());

			for icon in line.chars() {
				if !mapping.contains_key(&icon) {
					return Err(format!(
						"Missing key '{}' (there might be more)", icon
					));
				}
				row.push(icon);
			}

			matrix.push(row);
		}

		Ok(ConstructionLayout {
			matrix,
			mapping,
		})
	}

	pub fn height(&self) -> usize {
		self.matrix.len()
	}

	pub fn width(&self) -> usize {
		self.matrix[0].len()
	}
}

impl<'a> Generate for ConstructionLayout<'a> {
	fn generate(self) -> Self {
		self
	}
}

pub struct BuildingGen<'a> {
	hashmap: HashMap<String, ConstructionLayout<'a>>,
}

impl<'a> BuildingGen<'a> {
	pub fn generate() -> Self {
		let floor_stone = FoundationType::Floor(FoundationMaterial::Stone).into();
		let wall_stone = FoundationType::Wall(FoundationMaterial::Stone).into();
		
		let floor_wood = FoundationType::Floor(FoundationMaterial::Wood).into();
		let wall_wood = FoundationType::Wall(FoundationMaterial::Wood).into();

		let mut hashmap: HashMap<String, ConstructionLayout> = HashMap::new();

		let layout_hospital =
			"# # # # # # # # # # # # # # # # # # # # # # W # W # # # # #
			 # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ # _ _ _ # _ _ _ # _ _ _ W
			 # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ # B _ _ D _ _ _ D _ _ B #
			 # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ # _ _ _ # _ _ _ # _ _ _ W
			 # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ # # # # # _ _ _ # # # # #
			 # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ # _ _ _ # _ _ _ # _ _ _ W
			 # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ # B _ _ D _ _ _ D _ _ B #
			 # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ # _ _ _ # _ _ _ # _ _ _ W
			 # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ # # # # # _ _ _ # # # # #
			 # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ # _ _ _ # _ _ _ # _ _ _ W
			 # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ # B _ _ D _ _ _ D _ _ B #
			 # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ # _ _ _ # _ _ _ # _ _ _ W
			 # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ # # # # # _ _ _ # # # # #
			 # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ # _ _ _ # _ _ _ # _ _ _ W
			 # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ # B _ _ D _ _ _ D _ _ B #
			 # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ # _ _ _ # _ _ _ # _ _ _ W
			 # _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ # # # # # _ _ _ # # # # #
			 # _ _ _ _ _ _ _ # # # # # _ _ _ _ _ _ _ _ _ _ _ _ # _ _ _ W
			 # _ _ _ _ _ _ _ # _ _ _ # _ _ _ _ _ _ _ _ _ _ _ _ D _ _ B #
			 # _ _ _ _ _ _ _ D _ _ C X _ _ _ _ C C C C C _ _ _ # _ _ _ W
			 # # # # # # # # # # # # # # D D # # W # W # W # W # # # # #"
		;

		let mut mapping_hospital: ConstructionMapping = HashMap::new();
		mapping_hospital.insert(
			'#',
			ConstructionTile::new(wall_stone)
		);
		mapping_hospital.insert(
			'_',
			ConstructionTile::new(floor_stone)
		);
		mapping_hospital.insert(
			'D',
			ConstructionTile::new(floor_stone).with_item("DOOR_WOOD")
		);
		mapping_hospital.insert(
			'W',
			ConstructionTile::new(floor_stone).with_item("WINDOW_WOOD")
		);
		mapping_hospital.insert(
			'B',
			ConstructionTile::new(floor_stone).with_item("BED_WOOD")
		);
		mapping_hospital.insert(
			'C',
			ConstructionTile::new(floor_stone).with_item("CHAIR_WOOD")
		);
		mapping_hospital.insert(
			'X',
			ConstructionTile::new(floor_stone).with_item("COMPUTER")
		);

		hashmap.insert(
			"HOSPITAL".to_string(),
			ConstructionLayout::new(
				layout_hospital.to_string(), mapping_hospital
			).unwrap()
		);

		let layout_cozy_cottage =
			"# # # # # # # # # # # # # # #
			 # O _ _ # _ _ _ _ _ _ _ _ _ #
			 W _ _ _ D _ _ _ _ _ _ _ _ _ #
			 # _ _ _ # _ _ _ _ _ _ _ _ _ #
			 # # # # # # D # _ _ _ _ _ _ #
			 # X C O _ _ _ # O _ _ _ _ _ #
			 # _ _ _ _ _ _ # C T _ _ _ _ #
			 # _ _ _ _ _ _ # C T _ _ _ _ #
			 # B _ _ _ _ _ # _ _ _ _ _ _ #
			 # # # W # W # # # W # D # # #"
		;

		let mut mapping_cozy_cottage: ConstructionMapping = HashMap::new();
		mapping_cozy_cottage.insert(
			'#',
			ConstructionTile::new(wall_wood)
		);
		mapping_cozy_cottage.insert(
			'_',
			ConstructionTile::new(floor_wood)
		);
		mapping_cozy_cottage.insert(
			'D',
			ConstructionTile::new(floor_wood).with_item("DOOR_WOOD")
		);
		mapping_cozy_cottage.insert(
			'W',
			ConstructionTile::new(floor_wood).with_item("WINDOW_WOOD")
		);
		mapping_cozy_cottage.insert(
			'T',
			ConstructionTile::new(floor_wood).with_item("TABLE_WOOD")
		);
		mapping_cozy_cottage.insert(
			'C',
			ConstructionTile::new(floor_wood).with_item("CHAIR_WOOD")
		);
		mapping_cozy_cottage.insert(
			'B',
			ConstructionTile::new(floor_wood).with_item("BED_WOOD")
		);
		mapping_cozy_cottage.insert(
			'X',
			ConstructionTile::new(floor_wood).with_item("COMPUTER")
		);
		mapping_cozy_cottage.insert(
			'O',
			ConstructionTile::new(floor_wood).with_creature(
				"ORC",
				vec!(
					(AiPriority::Social, ChaseCreature::default())
			)
		));

		hashmap.insert(
			"COZY_COTTAGE".to_string(),
			ConstructionLayout::new(
				layout_cozy_cottage.to_string(), mapping_cozy_cottage
			).unwrap()
		);

		let layout_small_bedroom =
			"# # D # #
			 # _ _ L #
			 W C _ _ #
			 # T _ B #
			 # # # # #"
		;

		let mut mapping_small_bedroom: ConstructionMapping = HashMap::new();
		mapping_small_bedroom.insert(
			'_',
			ConstructionTile::new(floor_wood)
		);
		mapping_small_bedroom.insert(
			'#',
			ConstructionTile::new(wall_stone)
		);
		mapping_small_bedroom.insert(
			'W',
			ConstructionTile::new(floor_wood).with_item("WINDOW_WOOD")
		);
		mapping_small_bedroom.insert(
			'D',
			ConstructionTile::new(floor_wood).with_item("DOOR_WOOD")
		);
		mapping_small_bedroom.insert(
			'B',
			ConstructionTile::new(floor_wood).with_item("BED_WOOD")
		);
		mapping_small_bedroom.insert(
			'T',
			ConstructionTile::new(floor_wood).with_item("TABLE_WOOD")
		);
		mapping_small_bedroom.insert(
			'C',
			ConstructionTile::new(floor_wood).with_item("CHAIR_WOOD")
		);
		mapping_small_bedroom.insert(
			'L',
			ConstructionTile::new(floor_wood).with_item("LAMP")
		);

		hashmap.insert(
			"SMALL_BEDROOM".to_string(),
			ConstructionLayout::new(
				layout_small_bedroom.to_string(), mapping_small_bedroom
			).unwrap()
		);

		BuildingGen {
			hashmap,
		}
	}
}

impl<'a> Generator<'a, ConstructionLayout<'a>> for BuildingGen<'a> {
	fn hashmap(&self) -> &HashMap<String, ConstructionLayout<'a>> {
		&self.hashmap
	}
}