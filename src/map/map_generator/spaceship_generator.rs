use dastan_base::creature::ToCreature;
use dastan_base::creature::spaceship::Spaceship;

use crate::file_utils;
use crate::internet::Internet;

use super::{MapGenerator, UniverseInfo, MapInfo, GeneratorBundle};

mod my_spore_creation_1;
pub use my_spore_creation_1::MySporeCreation1;

pub trait SpaceshipMapGenerator<'a>: MapGenerator<'a> {
	fn new(
		universe_info: &'a UniverseInfo,
		gen_bundle: &'a GeneratorBundle,
		internet: &'a mut Internet,
		spaceship: &'a mut Spaceship
	) -> Self;

	fn map_dir_path(
		&self,
		universe_info: &UniverseInfo,
		spaceship: &Spaceship
	) -> String {
		file_utils::spaceship_dir(
			&universe_info.name,
			spaceship.name()
		)
	}
}