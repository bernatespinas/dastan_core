use dastan_base::{Computer, Generator, Identifier};
use dastan_base::creature::spaceship::Spaceship;
use dastan_base::item::furniture::passage::TeleportLocation;
use dastan_base::terrain::{FoundationMaterial, FoundationType};

use crate::internet::{Internet, Track};
use crate::{map::{Map, MapMode, Zone, Tile, ZonePosition, Position}};

use super::{GeneratorBundle, MapGenerator, SpaceshipMapGenerator, UniverseInfo, MapInfo};

pub struct MySporeCreation1<'a> {
	universe_info: &'a UniverseInfo,
	map_info: MapInfo,
	gen_bundle: &'a GeneratorBundle,
	internet: &'a mut Internet,
	spaceship: &'a mut Spaceship,
}

impl<'a> MySporeCreation1<'a> {
	fn fill_spaceship_zone(&mut self, zone: &mut Zone, map: &Map) {
		for y in zone.iter_rows() {
			for _x in zone.iter_cols() {
				zone.push_tile(y, Tile {
					creature: None,
					item: None,
					terrain_type: FoundationType::Floor(
						FoundationMaterial::Dirt
					).into(),
				});
			}
		}

		let computer: Computer = self.gen_bundle.generate("COMPUTER");
		let computer_zone_pos = ZonePosition {
			zone_id: (0, 0),
			pos: Position::new(6, 6),
		};
		self.internet.register(
			computer.id().unwrap(),
			map.map_file_path(),
			computer_zone_pos
		).unwrap();

		self.internet.link_device(
			computer.id().unwrap(),
			self.spaceship.id().unwrap()
		).unwrap();

		zone.tile_mut_yx(6, 6).item_put(computer);
		
		let teleporter = self.gen_bundle.gen_passage(
			"PLANET_TELEPORTER", 
			Some(TeleportLocation::NextToDevice(
				self.spaceship.id().unwrap()
			))
		);
		let teleporter_zone_pos = ZonePosition {
			zone_id: (0, 0),
			pos: Position::new(3, 3),
		};
		self.internet.register(
			teleporter.id().unwrap(),
			map.map_file_path(),
			teleporter_zone_pos
		).unwrap();

		self.spaceship.teleporter_id = teleporter.id();

		zone.tile_mut_yx(3, 3).item_put(teleporter);
	}
}

impl<'a> MapGenerator<'a> for MySporeCreation1<'a> {
	fn generate(mut self) -> Result<Map, String> {
		let mut map = Map::new(
			&self.map_info,
			self.map_dir_path(&self.universe_info, &self.spaceship)
		)?;

		debug!("y: 0/{}	,	x: 0/{}", map.map_height, map.map_width);
		let mut zone = Zone::with_capacity(10, 10);
		self.fill_spaceship_zone(&mut zone, &map);

		map.zone_insert((0, 0), zone);

		Ok(map)
	}
}

impl<'a> SpaceshipMapGenerator<'a> for MySporeCreation1<'a> {
	fn new(
		universe_info: &'a UniverseInfo,
		gen_bundle: &'a GeneratorBundle,
		internet: &'a mut Internet,
		spaceship: &'a mut Spaceship
	) -> Self {
		let map_info = MapInfo {
			name: "MySporeCreation1".to_string(),
			map_mode: MapMode::Simple,
		
			height: 1,
			width: 1,
		
			zone_height: 10,
			zone_width: 10,
		};

		MySporeCreation1 {
			universe_info,
			map_info,
			gen_bundle,
			internet,
			spaceship,
		}
	}
}