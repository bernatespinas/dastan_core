use dastan_base::{Biome, Generator, Identifier, Size};
use dastan_base::terrain::{TerrainType, FoundationType};
use dastan_base::item::furniture::passage::TeleportLocation;

use crate::map::map_generator::MapInfo;
use crate::internet::{Internet, Track};
use crate::{map::{Map, MapMode, Zone, ZonePosition, Position, building::BuildingGen}};

use super::{UniverseInfo, GeneratorBundle, MapGenerator, SpaceGenerator};
use super::super::{LaLaLand, PlanetGenerator};

pub struct Space<'a> {
	universe_info: &'a UniverseInfo,
	gen_bundle: &'a GeneratorBundle,
	building_gen: &'a BuildingGen<'a>,
	internet: &'a mut Internet,
}

impl<'a> Space<'a> {
	fn gen_planet_and_put_teleporter(
		&mut self,
		zone: &mut Zone,
		map: &Map
	) -> Result<(), String> {
		let earth01_info = MapInfo::new(
			"Earth01".to_string(),
			MapMode::ZonedLooping,
			Size::Tiny,
			self.universe_info.space_info.zone_height,
			self.universe_info.space_info.zone_width,
		);

		let earth01 = LaLaLand::new(
			self.universe_info,
			&earth01_info,
			self.gen_bundle,
			self.building_gen,
			&mut self.internet
		).generate()?;
		earth01.save_zones()?;

		let teleport = Some(
			TeleportLocation::Map(earth01.map_file_path())
		);
		let planet_teleporter = self.gen_bundle.gen_passage(
			"PLANET_TELEPORTER",
			teleport
		);
		zone.tile_mut_yx(3, 3).terrain_put(
			TerrainType::Foundation(FoundationType::Planet)
		);
		let teleporter_zone_pos = ZonePosition {
			zone_id: (0, 0),
			pos: Position::new(3, 3),
		};
		self.internet.register(
			planet_teleporter.id().unwrap(),
			map.map_file_path(),
			teleporter_zone_pos
		).unwrap();
		zone.tile_mut_yx(3, 3).item_put(planet_teleporter);

		Ok(())
	}
}

impl<'a> MapGenerator<'a> for Space<'a> {
	fn generate(mut self) -> Result<Map, String> {
		let mut map = Map::new(
			&self.universe_info.space_info,
			self.map_dir_path(&self.universe_info)
		)?;

		let space_biome: Biome = self.gen_bundle.generate("SPACE");

		for y in 0..map.map_height {
			for x in 0..map.map_width {
				debug!("y: {}/{}    ,    x: {}/{}", y, map.map_height, x, map.map_width);
				let mut zone = Zone::with_capacity(map.zone_height, map.zone_width);
				super::super::populate_zone_with_biome(
					&mut zone,
					&space_biome,
					&self.gen_bundle
				);

				if y == 0 && x == 0 {
					self.gen_planet_and_put_teleporter(&mut zone, &map)?;
				}

				map.zone_insert((y, x), zone);
			}
		}

		Ok(map)
	}
}

impl<'a> SpaceGenerator<'a> for Space<'a> {
	fn new(
		universe_info: &'a UniverseInfo,
		gen_bundle: &'a GeneratorBundle,
		building_gen: &'a BuildingGen<'a>,
		internet: &'a mut Internet
	) -> Space<'a> {
		Space {
			universe_info,
			gen_bundle,
			building_gen,
			internet,
		}
	}
}