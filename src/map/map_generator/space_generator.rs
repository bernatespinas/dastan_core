use crate::file_utils;
use crate::map::building::BuildingGen;
use crate::internet::Internet;

use super::{MapGenerator, UniverseInfo, GeneratorBundle};

mod space;
pub use space::Space;

pub trait SpaceGenerator<'a>: MapGenerator<'a> {
	fn new(
		universe_info: &'a UniverseInfo,
		gen_bundle: &'a GeneratorBundle,
		building_gen: &'a BuildingGen<'a>,
		internet: &'a mut Internet
	) -> Self;

	fn map_dir_path(&self, universe_info: &UniverseInfo) -> String {
		file_utils::space_dir(&universe_info.name)
	}
}