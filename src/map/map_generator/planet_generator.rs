use crate::file_utils;
use crate::map::building::BuildingGen;
use crate::internet::Internet;

use super::{MapGenerator, UniverseInfo, MapInfo, GeneratorBundle};

mod sea_depths;
pub use sea_depths::SeaDepths;

mod la_la_land;
pub use la_la_land::LaLaLand;

mod settlement;

pub trait PlanetGenerator<'a>: MapGenerator<'a> {
	fn new(
		universe_info: &'a UniverseInfo,
		map_info: &'a MapInfo,
		gen_bundle: &'a GeneratorBundle,
		building_gen: &'a BuildingGen<'a>,
		internet: &'a mut Internet
	) -> Self;

	fn map_dir_path(
		&self,
		universe_info: &UniverseInfo,
		map_info: &MapInfo
	) -> String {
		file_utils::planet_dir(&universe_info.name, &map_info.name)
	}
}