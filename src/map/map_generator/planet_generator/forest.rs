pub struct Forest {

}

impl<'a> Forest {
	fn place_terrain(&mut self, map: &mut Map<'a>) {
		let height = map.height as usize;
		let width = map.width as usize;

		for y in 0..height {
			map.grid.push(Vec::with_capacity(width));
			for _x in 0..width {
				map.grid[y].push(
					Tile {
						terrain: gen_bundle.gen_terrain("FLOOR_GRASS"),
						item: None,
						creature: None,
					}
				);
			}
		}
	}

	fn place_vegetation(&self, map: &mut Map<'a>, room: &Rectangle) {
		for y in room.y1..room.y2 {
			for x in room.x1..room.x2 {
				if y == x {
					map.item_put_yx(y, x, gen_bundle.gen_plant("PINE"));
				}
			}
		}
	}

	fn place_player(&self, map: &mut Map<'a>, _room: &Rectangle) {
		let y: usize = 10;
		let x: usize = 10;
		map.player_pos = Position{y: y as usize, x: x as usize};
	}
}

impl<'a> MapGenerator<'a> for Forest {
	fn new() -> Forest {
		Forest {

		}
	}

	fn generate(&mut self, world: &'a World, gen_bundle: &GeneratorBundle) -> Map<'a> {
		let height = world.height as usize;
		let width = world.width as usize;

		let mut map = Map::new(height as u16, width as u16);

		self.place_terrain(&mut map);
		self.place_vegetation(&mut map, &Rectangle::new(0, 0, height, width));

		self.place_player(&mut map, &Rectangle::new(0, 0, 0, 0));

		map
	}
}