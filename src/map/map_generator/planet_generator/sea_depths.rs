use dastan_base::{Biome, Generator, Identifier, Spaceship};

use crate::internet::{Internet, Track};
use crate::map::{Map, Position, Zone, ZonePosition};
use crate::map::building::BuildingGen;

use super::{UniverseInfo, MapInfo, GeneratorBundle, MapGenerator, PlanetGenerator};
use super::super::{SpaceshipMapGenerator, MySporeCreation1};

pub struct SeaDepths<'a> {
	universe_info: &'a UniverseInfo,
	map_info: &'a MapInfo,
	gen_bundle: &'a GeneratorBundle,
	internet: &'a mut Internet,
}

impl<'a> SeaDepths<'a> {
	fn gen_and_put_spaceship(
		&mut self,
		zone: &mut Zone,
		planet_map_file_path: String
	) -> Result<(), String> {
		let mut spaceship: Spaceship = self.gen_bundle.generate(
			"MySporeCreation1"
		);

		let spaceship_zone_pos = ZonePosition {
			zone_id: (0, 0),
			pos: Position::new(4, 4),
		};
		self.internet.register(
			spaceship.id().unwrap(),
			planet_map_file_path,
			spaceship_zone_pos
		).unwrap();

		let spaceship_map = MySporeCreation1::new(
			&self.universe_info,
			&self.gen_bundle,
			&mut self.internet,
			&mut spaceship
		).generate()?;
		spaceship_map.save_zones()?;

		zone.tile_mut_yx(4, 4).creature_put(spaceship);

		Ok(())
	}
}

impl<'a> MapGenerator<'a> for SeaDepths<'a> {
	fn generate(mut self) -> Result<Map, String> {
		let mut map = Map::new(
			&self.map_info,
			self.map_dir_path(&self.universe_info, &self.map_info)
		)?;

		let sea_depths_biome: Biome = self.gen_bundle.generate("SEA_DEPTHS");

		for y in 0..map.map_height {
			for x in 0..map.map_width {
				debug!("y: {}/{}    ,    x: {}/{}", y, map.map_height, x, map.map_width);
				let mut zone = Zone::with_capacity(map.zone_height, map.zone_width);
				super::super::populate_zone_with_biome(
					&mut zone,
					&sea_depths_biome,
					&self.gen_bundle
				);

				if y == 0 && x == 0 {
					self.gen_and_put_spaceship(&mut zone, map.map_file_path())?;
				}

				map.zone_insert((y, x), zone);
			}
		}

		Ok(map)
	}
}

impl<'a> PlanetGenerator<'a> for SeaDepths<'a> {
	fn new(
		universe_info: &'a UniverseInfo,
		map_info: &'a MapInfo,
		gen_bundle: &'a GeneratorBundle,
		_building_gen: &'a BuildingGen<'a>,
		internet: &'a mut Internet
	) -> SeaDepths<'a> {
		SeaDepths {
			universe_info,
			map_info,
			gen_bundle,
			internet,
		}
	}
}