use std::collections::HashMap;

use nrustes::{screen_height, screen_width};

use dastan_base::{Biome, Position};

use super::{WorldInfo, ZoneInfo};
use super::super::{
	Map, Zone, MapGenerator,
	Tile,
	GeneratorBundle,
};

const NUM_ZONES: usize = 20;

pub struct Continent<'a> {
	world_info: &'a WorldInfo,
	gen_bundle: &'a GeneratorBundle<'a>,
}

impl<'a> MapGenerator<'a> for Continent<'a> {
	fn new(
		world_info: &'a WorldInfo,
		gen_bundle: &'a GeneratorBundle<'a>
	) -> Continent<'a> {
		Continent {
			world_info,
			gen_bundle,
		}
	}

	fn gen(&mut self) -> Map {
		let mut zones: HashMap<u8, Zone> = HashMap::with_capacity(
			// self.world_info.size
			NUM_ZONES
		);
		let mut id = 0;
		let zone_info = ZoneInfo {
			biome: &self.gen_bundle.gen_biome("TEMPERATE_FOREST"),
			// biome: "FOREST_TEMPERATE",
		};

		for y in 0..screen_height() {
			for x in 0..screen_width() {
				let zone = self.gen_zone(&zone_info);
				zones.insert(id, zone);
				id += 1;
			}
		}

		let current_zone = zones.remove(&0).unwrap();
		Map {
			name: self.world_info.name.to_owned(),

			zones,

			current_zone,
			current_id: 0,

			player_pos: Position::new(20, 20),
			player_target: None,
		}
	}

	fn gen_zone(&mut self, zone_info: &ZoneInfo) -> Zone {
		let mut zone = Zone::with_capacity(
			screen_height() as usize
		);

		for y in 0..zone.grid.capacity() {
			let mut row: Vec<Tile> = Vec::with_capacity(
				screen_width() as usize
			);

			for x in 0..row.capacity() {
				row.push(Tile {
					creature: None,
					item: None,
					terrain: self.gen_bundle.gen_terrain(
						&zone_info.biome.terrain
					),
				});
			}

			zone.grid.push(row);
		}

		zone
	}
}