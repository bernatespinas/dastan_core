use dastan_base::GeneratorBundle;

use crate::map::Map;
use crate::map::position::ZonePosition;

pub mod village;

pub trait Settlement {
	fn deploy(
		&self,
		// template: Template,
		map: &mut Map,
		top_left: &ZonePosition,
		gen_bundle: &GeneratorBundle
	) -> Result<(), String>;
}