use std::collections::HashMap;
use std::ops::Range;

use rand::Rng;

use dastan_base::{Biome, Generator, Identifier, Spaceship};

use crate::internet::{Internet, Track};
use crate::map::{Map, Position, Zone, ZonePosition};
use crate::map::building::BuildingGen;

use super::super::{SpaceshipMapGenerator, MySporeCreation1};
use super::{UniverseInfo, MapInfo, GeneratorBundle, MapGenerator, PlanetGenerator};

use super::settlement::{
	Settlement,
	village::Village,
};

fn random_y_x(range_y: &Range<u16>, range_x: &Range<u16>) -> (u16, u16) {
	let random_y: u16 = rand::thread_rng().gen_range(
		range_y.clone()
	);
	let random_x: u16 = rand::thread_rng().gen_range(
		range_x.clone()
	);

	(random_y, random_x)
}

pub struct LaLaLand<'a> {
	universe_info: &'a UniverseInfo,
	map_info: &'a MapInfo,
	gen_bundle: &'a GeneratorBundle,
	building_gen: &'a BuildingGen<'a>,
	internet: &'a mut Internet,

	top_ice: Range<u16>,
	top_temperate: Range<u16>,
	bot_ice: Range<u16>,
	bot_temperate: Range<u16>,
}

impl<'a> LaLaLand<'a> {
	fn assign_biomes(&self) -> HashMap<Range<u16>, Biome> {
		let temperate_forest: Biome = self.gen_bundle.generate(
			"FOREST_TEMPERATE"
		);
		let ice_steppes: Biome = self.gen_bundle.generate("ICE_STEPPES");

		let mut biomes: HashMap<Range<u16>, Biome> = HashMap::new();
		biomes.insert(self.top_ice.clone(), ice_steppes.clone());
		biomes.insert(self.top_temperate.clone(), temperate_forest.clone());
		biomes.insert(self.bot_ice.clone(), ice_steppes);
		biomes.insert(self.bot_temperate.clone(), temperate_forest);

		biomes
	}

	fn gen_and_put_spaceship(
		&mut self,
		zone: &mut Zone,
		planet_map_file_path: String
	) -> Result<(), String> {
		let mut spaceship: Spaceship = self.gen_bundle.generate(
			"MySporeCreation1"
		);

		let spaceship_zone_pos = ZonePosition {
			zone_id: (0, 0),
			pos: Position::new(4, 4),
		};
		self.internet.register(
			spaceship.id().unwrap(),
			planet_map_file_path,
			spaceship_zone_pos
		).unwrap();

		let spaceship_map = MySporeCreation1::new(
			&self.universe_info,
			&self.gen_bundle,
			&mut self.internet,
			&mut spaceship
		).generate()?;
		spaceship_map.save_zones()?;

		zone.tile_mut_yx(4, 4).creature_put(spaceship);

		Ok(())
	}

	fn deploy_village(&mut self, map: &mut Map) -> Result<(), String> {
		let (random_temperate_y, random_temperate_x) = random_y_x(
			&self.top_temperate,
			&(0..map.map_width)
		);

		// debug!("Putting hospital in (zone_id, zone_pos) = (({}, {}), (0, 0)) aka pos {}", random_temperate_y, random_temperate_x, map.zone_pos_to_pos(&ZonePosition::new((random_temperate_y, random_temperate_x), Position::new(0, 0))));
		// // let hospital = self.gen_bundle.building_gen.get("HOSPITAL");
		// map.deploy_building(
		// 	&(random_temperate_y, random_temperate_x),
		// 	0,
		// 	0,
		// 	&self.gen_bundle.gen_building("HOSPITAL"),
		// 	&self.gen_bundle
		// );

		let random_zone_pos = ZonePosition::new(
			(random_temperate_y, random_temperate_x),
			Position::new(0, 0)
		);

		debug!(
			"Putting village in (zone_id, zone_pos) = (({}, {}), (0, 0)) aka pos {}",
			random_temperate_y,
			random_temperate_x,
			map.zone_pos_to_pos(&random_zone_pos)
		);

		let village = Village::new(&self.building_gen);
		village.deploy(
			map,
			&random_zone_pos,
			&self.gen_bundle
		)
	}

	// fn deploy_ponds(&mut self, map: &mut Map) {
	// 	let mut random_zones_ids = HashSet::new();

	// 	for _ in 0..40 {
	// 		let random_zone_id = random_y_x(
	// 			&self.top_temperate,
	// 			&(0..map.map_width)
	// 		);

	// 		if !random_zones_ids.contains(&random_zone_id) {
				

	// 			random_zones_ids.insert(random_zone_id);
	// 		}
	// 	}
	// }
}

impl<'a> MapGenerator<'a> for LaLaLand<'a> {
	fn generate(mut self) -> Result<Map, String> {
		let mut map = Map::new(
			&self.map_info,
			self.map_dir_path(&self.universe_info, &self.map_info),
		)?;

		let biomes = self.assign_biomes();

		for (range, biome) in biomes {
			for y in range {
				for x in 0..map.map_width {
					let mut zone = Zone::with_capacity(
						map.zone_height, map.zone_width
					);
					super::super::populate_zone_with_biome(
						&mut zone,
						&biome,
						&self.gen_bundle
					);

					if y == 0 && x == 0 {
						self.gen_and_put_spaceship(
							&mut zone,
							map.map_file_path()
						)?;
					}
					map.zone_insert((y, x), zone);
				}
			}
		}

		self.deploy_village(&mut map)?;

		Ok(map)
	}
}

impl<'a> PlanetGenerator<'a> for LaLaLand<'a> {
	fn new(
		universe_info: &'a UniverseInfo,
		map_info: &'a MapInfo,
		gen_bundle: &'a GeneratorBundle,
		building_gen: &'a BuildingGen<'a>,
		internet: &'a mut Internet
	) -> LaLaLand<'a> {
		let temperate_zones_height = map_info.height / 3;

		let ice_steppes_zones_height = temperate_zones_height / 2;

		let top_ice = 0..ice_steppes_zones_height;
		let top_temperate = top_ice.end..top_ice.end+temperate_zones_height;
		let bot_ice = top_temperate.end..top_temperate.end+ice_steppes_zones_height;
		let bot_temperate = bot_ice.end..bot_ice.end+temperate_zones_height;

		LaLaLand {
			universe_info,
			map_info,
			gen_bundle,
			building_gen,
			internet,

			top_ice,
			top_temperate,
			bot_ice,
			bot_temperate,
		}
	}
}