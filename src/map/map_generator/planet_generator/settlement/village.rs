use direction_simple::Direction;

use dastan_base::{Generator, GeneratorBundle};

use crate::map::{Map, ZonePosition, construction_deployer};
use crate::map::building::{ConstructionLayout, BuildingGen};

use super::Settlement;

// enum Template {
// 	AroundSquare,
// 	AroundPath,
// }

pub struct Village<'a> {
	cozy_cottage: ConstructionLayout<'a>,
	hospital: ConstructionLayout<'a>,
}

impl<'a> Village<'a> {
	pub fn new(building_gen: &'a BuildingGen<'a>) -> Village<'a> {

		Village {
			cozy_cottage: building_gen.generate("COZY_COTTAGE"),
			hospital: building_gen.generate("HOSPITAL"),
		}
	}
}

impl<'a> Settlement for Village<'a> {
	fn deploy(
		&self,
		// template: Template,
		map: &mut Map,
		top_left: &ZonePosition,
		gen_bundle: &GeneratorBundle
	) -> Result<(), String> {
		let mut current_top_left = *top_left;

		let update_current_top_left = |map: &Map, zone_pos: &ZonePosition| {
			map.pos_apply_dir_amount(
				zone_pos,
				Direction::Right,
				self.cozy_cottage.width()
			).unwrap()
		};

		construction_deployer::deploy(
			map,
			&current_top_left,
			&self.cozy_cottage,
			gen_bundle
		)?;

		current_top_left = update_current_top_left(&map, &current_top_left);

		construction_deployer::deploy(
			map,
			&current_top_left,
			&self.hospital,
			gen_bundle
		)
	}
}