use nrustes::Color;

use dastan_base::Creature;

use crate::Game;
use crate::map::ZonePosition;

#[derive(Copy, Clone, Serialize, Deserialize)]
pub enum Ai {
	BasicEnemy(BasicEnemy),
	CrazyChameleon(CrazyChameleon),
}

#[derive(Copy, Clone, Serialize, Deserialize)]
pub struct BasicEnemy {}

#[derive(Copy, Clone, Serialize, Deserialize)]
pub struct CrazyChameleon {
	pub crazy_color: Color,
}

impl Default for CrazyChameleon {
	fn default() -> CrazyChameleon {
		CrazyChameleon {
			crazy_color: Color::LBlue,
		}
	}
}

pub trait TakeTurn {
	fn take_turn(&mut self, zone_pos: ZonePosition, creature: &mut Creature, game: &mut Game) -> ZonePosition;
}

impl TakeTurn for Ai {
	fn take_turn(&mut self, zone_pos: ZonePosition, creature: &mut Creature, game: &mut Game) -> ZonePosition {
		match self {
			Ai::CrazyChameleon(ai) => ai.take_turn(zone_pos, creature, game),
			Ai::BasicEnemy(ai) => ai.take_turn(zone_pos, creature, game),
		};

		zone_pos
	}
}

impl TakeTurn for CrazyChameleon {
	fn take_turn(&mut self, zone_pos: ZonePosition, creature: &mut Creature, _game: &mut Game) -> ZonePosition {
		match creature {
			Creature::Humanoid(humanoid) => {
				let temp_color = humanoid.color;
				humanoid.color = self.crazy_color;
				self.crazy_color = temp_color;
			},
			Creature::Animal(animal) => {
				let temp_color = animal.color;
				animal.color = self.crazy_color;
				self.crazy_color = temp_color;
			},
			Creature::Spaceship(..) => {},
		};

		zone_pos
	}
}

impl TakeTurn for BasicEnemy {
	fn take_turn(&mut self, zone_pos: ZonePosition, _creature: &mut Creature, _game: &mut Game) -> ZonePosition {
		zone_pos
	}
}