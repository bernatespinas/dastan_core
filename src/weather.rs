// TODO Maybe use style::values::specified::percentage::Percentage?
struct Chance {
	chance: u8,
}

impl Chance {
	pub fn new(chance: u8) -> Result<Ok(Chance), Err()> {
		if chance > 100 {
			Err(())
		}
		else {
			Ok(
				Chance {
					chance,
				}
			)
		}
	}
}

enum Strength {
	Weak,
	Normal,
	Strong,
}

// struct WeatherThingy {
// 	strength: Strength,
// 	chance: Chance,
// }

enum WeatherThingy {
	Sun(chance: Chance, strength: Strength),
	Wind(chance: Chance, strength: Strength),
	Cloud(chance: Chance, strength: Strength),
	Rain(chance: Chance, strength: Strength),
	Fog(chance: Chance, strength: Strength),
}

enum Temperature {
	Freezing,
	Cold,
	Cool,
	Temperate,
	Warm,
	Hot,
	Scorching,
}

// TODO A Vec<WeatherThingy> with only the relevant thingies? Or a "struct attribute" (check how they're called) for each one?
pub struct Weather {
	// TODO It makes more sense for Season to be in Wheter than in Time, right?
	season: Season,
	temperature: Temperature,//TODO i16,
	sky: Sky,
	wind: Wind,
}

impl Weather {
	pub fn load_biome(&mut self, biome: &Biome) {

	}

	pub fn from_biome(biome: &Biome) -> Weather {

	}

	pub fn next_season(&mut self) {
		self.season = self.season.next();
	}
}