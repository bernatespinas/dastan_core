use dastan_base::time::{Hour, Day, Month, Season, PartOfDay};

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Time {
	pub hour: Hour,
	pub day: Day,
	pub month: Month,
	pub year: u16,
}

impl Default for Time {
	fn default() -> Time {
		Time {
			hour: Hour::default(),
			day: Day::default(),
			month: Month::January,
			year: 0,
		}
	}
}

impl Time {
	/// Tickes a minute, which might tick an hour, which might tick a day. ___
	pub fn tick_minute(&mut self) -> bool {
		self.hour.next_minute();
		// if self.hour.hour == 0 && self.hour.minute == 0 {
			// self.next_day();
		// }

		// If minute == 0, it's been an hour!
		if self.hour.minute() == 0 {
			// If hour == 0, it's also been a day!
			if self.hour.hour() == 0 {
				self.next_day();
			}

			return true;
		}

		false
	}

	fn next_day(&mut self) {
		self.day.next();
		if self.day.number() == 1 {
			self.next_month();
		}
	}

	fn next_month(&mut self) {
		self.month = Month::next(self.month);
		if let Month::January = self.month {
			self.next_year();
		}
	}

	fn next_year(&mut self) {
		self.year += 1;
	}

	fn season(&self) -> Season {
		Season::from(self.month)
	}

	pub fn part_of_day(&self) -> PartOfDay {
		self.season().part_of_day(self.hour.hour())
	}

	// Sleeping should add hours until day time or something...
// 	fn add_hour(&mut self, i: u8) {
// 		self.hour += i;
// 		// TODO
// 	}

	// fn new(hour: u8, day: Day, month: Month, year: u16) -> Time {
	// 	Time {
	// 		hour: Hour::new(hour, 0).unwrap(),
	// 		day,
	// 		month,
	// 		year,
	// 	}
	// 	// debug_assert!(hour <= 23);
	// 	// let season = Season::from(&month);
	// 	// Time {hour, day, month, year, season}
	// }

	pub fn day_number(&self) -> u8 {
		self.day.number()
	}

// 	fn update_hour(&self) {
// 		// if self.hour == 8 {
// 		// 	self.good_morning();
// 		// }
// 		// else if self.hour == 14 {
// 		// 	self.good_afternoon();
// 		// }
// 		// else if self.hour == 21 {
// 		// 	self.good_night();
// 		// }
//
// 		match self.hour {
// 			8	=> self.good_morning(),
// 			14	=> self.good_afternoon(),
// 			21	=> self.good_night(),
// 			_	=> ()
// 		};
// 	}
//
// 	fn good_morning(&self) {
// // 		nrustes::initialize_colors(-1);
// 	}
//
// 	fn good_afternoon(&self) {
// // 		nrustes::initialize_colors(1);
// 	}
// 	fn good_night(&self) {
// // 		nrustes::initialize_colors(-2);
// 	}

// 	fn next_day(&)
}