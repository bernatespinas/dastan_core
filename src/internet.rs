use std::collections::HashMap;

use dastan_base::{DeviceId, LivingId, UuidIdentifier};

use crate::map::{MapZonePos, ZonePosition};

#[derive(Serialize, Deserialize)]
pub struct Internet {
	devices: HashMap<DeviceId, MapZonePos>,
	livings: HashMap<LivingId, MapZonePos>,

	linked_devices: HashMap<DeviceId, Vec<DeviceId>>,
}

impl Default for Internet {
	fn default() -> Internet {
		Internet {
			devices: HashMap::new(),
			livings: HashMap::new(),

			linked_devices: HashMap::new(),
		}
	}
}

pub trait Track<T: Eq + std::hash::Hash + UuidIdentifier> {
	fn hashmap(&self) -> &HashMap<T, MapZonePos>;

	fn hashmap_mut(&mut self) -> &mut HashMap<T, MapZonePos>;

	/// Register a device in a specific map and position. If that device_id is already present, return its currently stored map_file_path and zone_pos.
	fn register(
		&mut self,
		t: T,
		map_file_path: String,
		zone_pos: ZonePosition
	) -> Result<(), MapZonePos> {
		let uuid = t.uuid().clone();

		match self.hashmap_mut().insert(
			t,
			MapZonePos {
				map_file_path: map_file_path.clone(),
				zone_pos: zone_pos,
			}
		) {
			Some(already_registered_pos) => {
				debug!(
					"Tried to register '{}' at map '{}' and zone_pos '{:?}', but it was already registered at map '{}' and zone_pos '{:?}'",
					uuid,
					map_file_path,
					zone_pos,
					already_registered_pos.map_file_path,
					already_registered_pos.zone_pos
				);

				Err(already_registered_pos)
			},
			None => Ok(())
		}
	}

	fn unregister(
		&mut self,
		t: &T
	) -> Result<MapZonePos, ()> {
		match self.hashmap_mut().remove(t) {
			Some(map_zone_pos) => Ok(map_zone_pos),
			None => {
				debug!("Tried to unregister '{}', but it hadn't been registered", t.uuid());

				Err(())
			},
		}
	}

	fn update(
		&mut self,
		t: T,
		map_file_path: String,
		zone_pos: ZonePosition
	) -> Result<(), ()> {
		self.unregister(&t)?;
		self.register(t, map_file_path, zone_pos)
			.map_err(|_| ())
	}

	fn location<'a>(&'a self, t: &'a T) -> Option<&MapZonePos> {
		self.hashmap().get(t)
	}

	fn is_registered(&self, t: &T) -> bool {
		self.hashmap().contains_key(t)
	}
}

impl Track<DeviceId> for Internet {
	fn hashmap(&self) -> &HashMap<DeviceId, MapZonePos> {
		&self.devices
	}

	fn hashmap_mut(&mut self) -> &mut HashMap<DeviceId, MapZonePos> {
		&mut self.devices
	}
}

impl Track<LivingId> for Internet {
	fn hashmap(&self) -> &HashMap<LivingId, MapZonePos> {
		&self.livings
	}
	
	fn hashmap_mut(&mut self) -> &mut HashMap<LivingId, MapZonePos> {
		&mut self.livings
	}
}

impl Internet {
	pub fn list_linked_devices(
		&self,
		device_id: &DeviceId
	) -> Option<&Vec<DeviceId>> {
		self.linked_devices.get(device_id)
	}

	pub fn link_device(
		&mut self,
		linker_device_id: DeviceId,
		linked_device_id: DeviceId
	) -> Result<(), String> {
		if !self.is_registered(&linker_device_id) {
			debug!("Tried to link device linked to device linker: {}, but linker hadn't been registered", linker_device_id);

			Err(format!("Linker device {} not registered", linker_device_id))
		} else if !self.is_registered(&linked_device_id) {
			debug!("Tried to link device linked: {} to device linker, but linked hadn't been registered", linked_device_id);

			Err(format!("Linked device {} not registered", linked_device_id))
		} else {
			let linked_devices = self.linked_devices
				.entry(linker_device_id)
				.or_insert(Vec::new())
			;
			linked_devices.push(linked_device_id);
			Ok(())
		}
	}
}