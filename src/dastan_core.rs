#![feature(box_syntax, box_patterns)]

#[macro_use] extern crate serde;
#[macro_use] extern crate log;

pub mod game;
pub mod map;
pub mod internet;
pub mod message_log;
// mod ai;
pub mod ai_struct_tasks;
pub use ai_struct_tasks as ai;
pub mod time;
pub mod command;
pub mod debug_logger;
pub mod file_utils;