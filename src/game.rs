use std::collections::{HashSet, VecDeque};

use direction_simple::Direction;

use dastan_base::GeneratorBundle;
use dastan_base::terrain::TerrainType;
use dastan_base::creature::{Creature, ToCreature};

use crate::internet::Internet;
use crate::map::{Map, Tile, ZonePosition, MapZonePos, Position, MapManager};
use crate::map::building::BuildingGen;
use crate::map::pathfinding::Pathfinding;
use crate::map::map_generator::UniverseInfo;
use crate::time::Time;
use crate::message_log::MessageLog;
use crate::debug_logger::DebugLogger;

use crate::file_utils;

mod ai_system;
use ai_system::AiSystem;

pub mod move_or_attack;
// mod movement_fn;

// TODO
#[derive(PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct Location {}

/// Diferent, uh, actions an, ehm..., action can ¿take?
#[derive(Debug, Clone)]
pub enum GameAction {
	TakeTurn,
	Redraw,
	TakeTurnAndRedraw,
	NotTakeTurn,
	Move(ZonePosition, Direction),
	MoveAndRedraw(ZonePosition, Direction),
	ChangeMap(ChangeMapBundle),
	PilotSpaceship(ChangeMapBundle),
	StopPilotingSpaceship,
	Save,
	Exit,
}

impl GameAction {
	/// Returns true if the aaction takes a turn.
	pub fn take_turn(&self) -> bool {
		match self {
			GameAction::TakeTurn
			| GameAction::TakeTurnAndRedraw
			| GameAction::Move(..)
			| GameAction::MoveAndRedraw(..)
			| GameAction::ChangeMap(..)
			| GameAction::PilotSpaceship(..)
			| GameAction::StopPilotingSpaceship
			=> true,
			_ => false,
		}
	}

	pub fn redraw(&self) -> bool {
		match self {
			GameAction::Redraw
			| GameAction::TakeTurnAndRedraw
			| GameAction::MoveAndRedraw(..)
			| GameAction::ChangeMap(..)
			| GameAction::PilotSpaceship(..)
			| GameAction::StopPilotingSpaceship
			=> true,
			_ => false,
		}
	}

	// TODO If the GameAction has a redraw variant, return it. What should I do if it doesn't?
	pub fn also_redraw(self) -> GameAction {
		match self {
			GameAction::TakeTurn => GameAction::TakeTurnAndRedraw,
			GameAction::NotTakeTurn => GameAction::Redraw,
			GameAction::Move(pos, dir) => GameAction::MoveAndRedraw(pos, dir),
			whatevs => whatevs
		}
	}
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ChangeMapBundle {
	pub map_zone_pos: MapZonePos,
	pub player_target: Option<ZonePosition>,
	pub map_pad_top_left: Option<ZonePosition>,
}

impl ChangeMapBundle {
	pub fn new(
		map_file_path: String,
		zone_pos: ZonePosition
	) -> ChangeMapBundle {
		ChangeMapBundle {
			map_zone_pos: MapZonePos {
				map_file_path,
				zone_pos,
			},
			player_target: None,
			map_pad_top_left: None,
		}
	}

	pub fn map_file_path(&self) -> &str {
		&self.map_zone_pos.map_file_path
	}

	pub fn player_zone_pos(&self) -> &ZonePosition {
		&self.map_zone_pos.zone_pos
	}
}

#[derive(Serialize, Deserialize)]
pub struct Game {
	name: String,

	/// Whether that Game hasn't been played yet for the first time.
	pub new_game: bool,

	#[serde(skip)]
	pub map_manager: MapManager,
	/// The path of the Map file the player is currently in.
	pub map_file_path: String,
	/// The player's ZonePosition in the current/active Map.
	pub player_zone_pos: ZonePosition,

	pub top_left: ZonePosition,

	/// Maps the player was in OMG TODO
	pub map_stack: VecDeque<ChangeMapBundle>,

	pub message_log: MessageLog,
	pub time: Time,
	pub internet: Internet,

	pub player_target: Option<ZonePosition>,

	// The top left position of the MapPad. Since MapPad isn't saved, I'm keeping that value here.
	// pub top_left: ZonePosition,

	// TODO!
	// bg_tasks: Vec<Task>,

	// I generate everything (just Items for now) when loading any Game... I
	// still don't know if I should let each game keep the generators it had
	// when it was created. TODO
		// Hmm, I guess I should always use the current generators. If someone
		// wants to keep playing a save created with an older version of the
		// game, the best idea would be to keep the older version for that!
	#[serde(skip)]
	pub gen_bundle: GeneratorBundle,

	#[serde(skip)]
	pub tiles_pending_redraw: HashSet<ZonePosition>,

	#[serde(skip)]
	pub logging: DebugLogger,
}

impl Game {
	pub fn new(
		universe_info: &UniverseInfo,
		gen_bundle: &GeneratorBundle,
		building_gen: &BuildingGen
	) -> Result<Game, String> {
		let mut internet = Internet::default();

		// let map = Map::with_info(universe_info);
		let map = universe_info.generate_map(gen_bundle, building_gen, &mut internet)?;
		let map_file_path = map.map_file_path();
		debug!("created map at {}", map.map_file_path());

		// TODO
		let player_zone_pos = ZonePosition::new(
			(0, 0),
			Position::new(0, 0)
		);

		// let top_left = MapPad::initial_coordinates(
		// 	&map,
		// 	&player_zone_pos
		// );
		let top_left = player_zone_pos;

		let mut map_manager = MapManager::default();
		map_manager.insert_map(map);

		let message_log = MessageLog::new();
		let time = Time::default();

		Ok(Game {
			name: universe_info.name.to_string(),
			new_game: true,

			map_manager,
			map_file_path,
			player_zone_pos,

			map_stack: VecDeque::new(),

			message_log,
			time,
			internet,

			player_target: None,

			top_left,

			gen_bundle: GeneratorBundle::default(),

			tiles_pending_redraw: HashSet::new(),

			logging: DebugLogger::default(),
		})
	}

	pub fn load(name: &str) -> Result<Game, String> {
		let game_file = file_utils::read(
			&file_utils::universe_file(name),
			"universe"
		)?;

		let mut game: Game = file_utils::deserialize(
			&game_file,
			"game (version mismatch?)"
		)?;

		debug!("loaded game that says main map is at map_dir {}", game.map_file_path);
		game.map_manager.load_map(&game.map_file_path)?;

		let player_zone_id = game.player_zone_pos.zone_id;
		game.map_mut().load_initial_zones(&player_zone_id)?;

		Ok(game)
	}

	pub fn save_universe(&self) -> Result<(), String> {
		file_utils::serialize_and_write(
			self,
			&file_utils::universe_file(&self.name),
			"universe"
		)?;

		// Save all the Zones that have been loaded, which are skipped
		// by #[serde(skip)] to keep them in separate files.
		// The currently loadded Zones are in Map, while the Zones that
		// have been unloaded are in .new files.
		self.map_manager.save_maps()
	}

	pub fn save_player(player: &Creature) -> Result<(), String> {
		file_utils::serialize_and_write(
			player,
			&file_utils::player_file(player.name()),
			"player"
		)
	}

	pub fn map(&self) -> &Map {
		self.map_manager.map(&self.map_file_path)
	}

	pub fn map_mut(&mut self) -> &mut Map {
		self.map_manager.map_mut(&self.map_file_path)
	}

	pub fn change_map(
		&mut self,
		change_map_bundle: &ChangeMapBundle
	) -> Result<(), String> {
		let map_file_path = change_map_bundle.map_file_path();
		let player_zone_pos = change_map_bundle.player_zone_pos();
	
		self.map_manager.load_map_and_required_zones(
			map_file_path,
			&player_zone_pos.zone_id
		)?;
		self.map_file_path = map_file_path.to_string();
		self.player_pos_update(&player_zone_pos);

		self.clear_player_target();

		Ok(())
	}

	pub fn set_player_target(&mut self, zone_pos: ZonePosition) {
		self.player_target = Some(zone_pos);
	}

	pub fn has_player_target(&self) -> bool {
		self.player_target.is_some()
	}

	pub fn clear_player_target(&mut self) -> Option<ZonePosition> {
		self.player_target.take()
	}

	pub fn is_player_target(&self, zone_pos: &ZonePosition) -> bool {
		match &self.player_target {
			Some(player_target_zone_pos) => player_target_zone_pos == zone_pos,
			None => false 
		}
	}

	// The current movement function. This "dictates" where the player can
	// move. For example, if the player is walking, only non-blocking floors
	// are valid; if the player is swimming, only liquid tiles are valid. If
	// the player is walking and wants/decides to swim, for example, this function will have to be changed.
	pub fn get_movement_fn(&self) -> move_or_attack::MovementFn {
		// debug!("called get_movement_fn with player at map {} and at pos {:?}", self.map_file_path, self.player_zone_pos);
		match self.tile_player().terrain_type {
			TerrainType::Foundation(..) => move_or_attack::walk,
			TerrainType::Liquid(..) => move_or_attack::swim,
		}
	}

	pub fn process_ais(
		&mut self,
		game_action: &GameAction,
		pathfinding: &mut Pathfinding,
	) {
		ai_system::CreatureAiSystem::process(
			self,
			pathfinding,
			game_action.redraw()
		);
	}

	pub fn player(&self) -> &Creature {
		self.tile_player().creature().as_ref()
			.unwrap_or_else(|| panic!("player not found"))
	}

	pub fn player_put<T>(&mut self, player: T)
	where
		T: ToCreature,
	{
		let player_zone_pos = self.player_zone_pos;

		// debug!("putting player {} in {:?}", player.name(), self.player_zone_pos);

		self.map_mut()
			.zone_tile_mut(&player_zone_pos)
			.creature_put(player)
		;
	}

	pub fn player_take(&mut self) -> Creature {
		let player_zone_pos = self.player_zone_pos;

		self.map_mut()
			.zone_tile_mut(&player_zone_pos)
			.creature_take()
			.unwrap_or_else(|| panic!("error getting player from current zone"))
	}

	pub fn tile_player(&self) -> &Tile {
		self.map().zone_tile(&self.player_zone_pos)
	}

	pub fn player_pos_update(&mut self, zone_pos: &ZonePosition) {
		self.player_zone_pos = *zone_pos;
	}

	pub fn player_pos(&self) -> Position {
		self.map().zone_pos_to_pos(&self.player_zone_pos())
	}

	pub fn player_zone_pos(&self) -> ZonePosition {
		self.player_zone_pos
	}

	pub fn is_adjacent_to_player(&self, zone_pos: &ZonePosition) -> bool {
		self.map().are_adjacent(
			zone_pos,
			&self.player_zone_pos
		)
	}

	pub fn is_player_pos(
		&self,
		zone_pos: &ZonePosition,
		map_file_path: &str
	) -> bool {
		&self.player_zone_pos == zone_pos
		&&
		self.map_file_path == map_file_path
	}

	pub fn is_current_map(&self, map: &Map) -> bool {
		self.map_file_path == map.map_file_path()
	}

	pub fn tiles_pending_redraw_take(&mut self) -> HashSet<ZonePosition> {
		std::mem::take(&mut self.tiles_pending_redraw)
	}
}