use std::collections::HashMap;
use std::sync::{Arc, RwLock};

use direction_simple::Direction;

use dastan_base::{Corpse, terrain::TerrainType};

use crate::ai::CreatureAi;

use crate::file_utils;

mod position;
pub use position::{Position, ZoneId, ZonePosition, MapZonePos};

mod zone;
pub use zone::Zone;

mod tile;
pub use tile::Tile;

mod map_manager;
pub use map_manager::MapManager;

mod zone_manager;
use zone_manager::{ZoneManager, SingleZoneManager, ZonedLoopingZoneManager};

pub mod map_generator;
use map_generator::MapInfo;

pub mod building;
mod construction_deployer;

pub mod pathfinding;

#[derive(Copy, Clone, PartialEq, Serialize, Deserialize)]
pub enum MapMode {
	/// A looping map divided in zones. Like the Earth: too huge to load at once, and if the player keeps moving to the same direction it/they will eventually arrive at the starting point.
	ZonedLooping,
	// Looping,
	// TODO A map divided in zones. For example, a building: each zone could be a story.
	Zoned,
	/// A map that is small enough to be loaded all at once. It's also not looping.
	Simple,
}

/// A map that is divided in Zones. This makes it easier to load and unload them.
#[derive(Serialize, Deserialize)]
pub struct Map {
	/// The name of the map, also used to know in which file it should be saved.
	name: String,
	dir_path: String,

	/// Number of Zone "rows".
	pub map_height: u16,
	/// Number of Zone "columns".
	pub map_width: u16,

	/// The height of each Zone, which should always be the same.
	pub zone_height: u16,
	/// The width of each Zone, which should always be the same.
	pub zone_width: u16,

	pub map_mode: MapMode,

	#[serde(skip)]
	/// The currently loaded and active Zones.
	zones: HashMap<ZoneId, Zone>,

	#[serde(skip)]
	/// Zones surrounding self.zones, inactive but ready to be used.
	extra_zones: Arc<RwLock<HashMap<ZoneId, Zone>>>,
}

impl Map {
	pub fn new(map_info: &MapInfo, dir_path: String) -> Result<Map, String> {
		debug!("new map will be at path {}", &dir_path);
		file_utils::create_dir_all(&dir_path, "new map")?;

		let zones_capacity = map_info.height * map_info.width;

		Ok(Map {
			name: map_info.name.to_string(),
			dir_path,

			map_mode: map_info.map_mode,

			// TODO
			map_height: map_info.height,
			map_width: map_info.width,

			zone_height: map_info.zone_height,
			zone_width: map_info.zone_width,
			
			zones: HashMap::with_capacity(zones_capacity as usize),
			extra_zones: Arc::new(RwLock::new(HashMap::new())),
		})
	}

	// TODO -1?
	pub fn height_total(&self) -> u16 {
		// (self.map_height as usize) * (self.zone_height-1)
		(self.map_height) * (self.zone_height)
	}

	// TODO -1?
	pub fn width_total(&self) -> u16 {
		// (self.map_width as usize) * (self.zone_width-1)
		(self.map_width) * (self.zone_width)
	}

	pub fn map_file_path(&self) -> String {
		format!("{}/{}", self.dir_path, self.name)
	}

	pub fn zone(&self, zone_id: &ZoneId) -> &Zone {
		self.zones.get(zone_id).unwrap()
	}

	pub fn zone_mut(&mut self, zone_id: &ZoneId) -> &mut Zone {
		self.zones.get_mut(zone_id).unwrap()
	}

	pub fn zone_tile(&self, zone_pos: &ZonePosition) -> &Tile {
		self.zones.get(&zone_pos.zone_id).unwrap().tile(&zone_pos.pos)
	}

	pub fn zone_tile_mut(&mut self, zone_pos: &ZonePosition) -> &mut Tile {
		self.zones.get_mut(&zone_pos.zone_id).unwrap().tile_mut(&zone_pos.pos)
	}

	pub fn add_zone(&mut self, zone_id: &ZoneId) -> Result<(), String> {
		let zone = file_utils::load_zone_path(
			&self.dir_path,
			zone_id
		)?;

		self.zones.insert(*zone_id, zone);

		Ok(())
	}

	pub fn zone_insert(&mut self, zone_id: ZoneId, zone: Zone) {
		self.zones.insert(zone_id, zone);
	}

	// TODO
	pub fn zone_overview_cost_matrix(&self) -> [[u8; 3]; 3] {
		[[0u8; 3]; 3]
	}

	pub fn load_initial_zones(
		&mut self,
		player_zone_id: &ZoneId
	) -> Result<(), String> {
		match self.map_mode {
			MapMode::Simple => {
				SingleZoneManager::load_initial_zones(self, player_zone_id)
			},
			MapMode::Zoned => todo!(),
			MapMode::ZonedLooping => {
				ZonedLoopingZoneManager::load_initial_zones(
					self,
					player_zone_id
				)
			},
		}
	}

	pub fn update_loaded_zones(
		&mut self,
		player_zone_id: &ZoneId
	) -> Result<(), String> {
		match self.map_mode {
			MapMode::Simple => {
				SingleZoneManager::update_loaded_zones(self, player_zone_id)
			},
			MapMode::Zoned => todo!(),
			MapMode::ZonedLooping => {
				ZonedLoopingZoneManager::update_loaded_zones(
					self,
					player_zone_id
				)
			}
		}
	}

	pub fn creature_ais_take(
		&mut self
	) -> HashMap<ZoneId, HashMap<Position, CreatureAi>> {
		let mut creature_ais = HashMap::with_capacity(self.zones.len());

		for (zone_id, zone) in &mut self.zones {
			creature_ais.insert(*zone_id, zone.creature_ais_take());
		}

		creature_ais
	}

	pub fn creature_ais_set(
		&mut self,
		creature_ais: HashMap<ZoneId, HashMap<Position, CreatureAi>>
	) {
		for (zone_id, zone_creature_ais) in creature_ais {
			self.zone_mut(&zone_id).creature_ais_set(zone_creature_ais);
		}
	}

	pub fn creature_ais_add(
		&mut self,
		zone_pos: ZonePosition,
		creature_ai: CreatureAi
	) {
		self
			.zone_mut(&zone_pos.zone_id)
			.creature_ais_add(zone_pos.pos, creature_ai)
		;
	}

	// pub fn del_zones_enough_distance(&mut self) {

	// }

	// TODO Use threads.
	pub fn save_zones(&self) -> Result<(), String> {
		// Create a directory for the Map if it doesn't exist, with a subfolder for its Zones.
		file_utils::create_dir_all(
			&format!("{}/zones/buffer", self.dir_path),
			"map and zones"
		)?;

		// Save the currently loaded Zones.
		for (zone_id, zone) in &self.zones {
			file_utils::save_zone_path_buffer(&self.dir_path, zone_id, zone)?;
		}

		// Save extra Zones.
		for (zone_id, zone) in self.extra_zones.read().unwrap().iter() {
			file_utils::save_zone_path_buffer(&self.dir_path, zone_id, zone)?;
		}
		
		// At this point all Zones have been saved successfully in the buffer dir.

		// Attempt to save the Map file.
		debug!("saving map {} in map_dir {}", self.name, self.dir_path);
		file_utils::serialize_and_write(
			&self,
			self.map_file_path(),
			"map"
		)?;

		// At this point everything has been saved successfully.

		// Move Zones' files from the buffer dir to the zones dir.
		file_utils::replace_buffer_zones(&self.dir_path)?;


		Ok(())
	}

	pub fn pos_apply_dir(&self, zone_pos: &ZonePosition, dir: Direction) -> Result<ZonePosition, ()> {
		match self.map_mode {
			MapMode::Simple => {
				SingleZoneManager::pos_apply_dir(
					self,
					zone_pos,
					dir
				)
			},
			MapMode::Zoned => todo!(),
			MapMode::ZonedLooping => {
				ZonedLoopingZoneManager::pos_apply_dir(
					self,
					zone_pos,
					dir
				)
			},
		}
	}

	/// Tries to apply a Direction to a ZonePosition $amount times. Returns the last valid ZonePosition.
	pub fn pos_apply_dir_amount(
		&self,
		zone_pos: &ZonePosition,
		dir: Direction,
		amount: usize
	) -> Result<ZonePosition, ZonePosition> {
		match self.map_mode {
			MapMode::Simple => {
				SingleZoneManager::pos_apply_dir_amount(
					self,
					zone_pos,
					dir,
					amount
				)
			},
			MapMode::Zoned => todo!(),
			MapMode::ZonedLooping => {
				ZonedLoopingZoneManager::pos_apply_dir_amount(
					self,
					zone_pos,
					dir,
					amount
				)
			},
		}
	}

	pub fn zone_pos_to_pos(&self, zone_pos: &ZonePosition) -> Position {
		let y = zone_pos.zone_id.0 * self.zone_height + zone_pos.pos.y;
		let x = zone_pos.zone_id.1 * self.zone_width + zone_pos.pos.x;

		Position {
			y,
			x
		}
	}

	pub fn creature_to_corpse(&mut self, zone_pos: &ZonePosition) {
		// TODO: (sigh) can't mutably borrow map because of past immutable borrows... (dead stare)
		// let creature = self.get_creature(y, x).unwrap();
		let zone = self.zone_mut(&zone_pos.zone_id);
		let creature = zone.tile_mut(&zone_pos.pos).creature_take().unwrap();

		if zone.tile(&zone_pos.pos).item_there() {
			// TODO
			// nrustes::Popup::error(&format!("WARNING: A Creature has died on an Item (y: {}, x: {}. This Item will be CRUSHED.", pos.y, pos.x));
			zone.tile_mut(&zone_pos.pos).item_take();
		}

		zone.tile_mut(&zone_pos.pos).item_put(Corpse::new(creature));
	}

	pub fn get_zone_y_up(&self, y: u16) -> u16 {
		if y == 0 {
			self.map_height-1
		} else {
			y-1
		}
	}

	fn get_zone_y_down(&self, y: u16) -> u16 {
		if y == self.map_height-1 {
			0
		} else {
			y+1
		}
	}

	pub fn get_zone_x_left(&self, x: u16) -> u16 {
		if x == 0 {
			self.map_width-1
		} else {
			x-1
		}
	}

	fn get_zone_x_right(&self, x: u16) -> u16 {
		if x == self.map_width-1 {
			0
		} else {
			x+1
		}
	}

	// pub fn get_zone_dir(&self, zone_id: &ZoneId, dir: Direction) -> &Zone {
	// 	self.zones.get(&self.zone_id_apply_dir(zone_id, dir)).unwrap()
	// }

	pub fn terrain_cover(
		&mut self,
		zone_pos: &ZonePosition,
		new_terrain_type: TerrainType
	) {
		self
			.zone_mut(&zone_pos.zone_id)
			.terrain_cover(zone_pos.pos, new_terrain_type)
		;
	}

	pub fn terrain_covered_there(&self, zone_pos: &ZonePosition) -> bool {
		self.zone(&zone_pos.zone_id).terrain_covered_there(&zone_pos.pos)
	}

	// TODO Umm, puc anar pels dos costats...
	// pub fn distance_between(&self, zone_pos: &ZonePosition, other: &ZonePosition) -> (usize, usize) {
	// 	let dist_y

	// 	let dist_x

	// 	(dist_y, dist_x)
	// }

	pub fn are_adjacent(
		&self,
		zone_pos_1: &ZonePosition,
		zone_pos_2: &ZonePosition
	) -> bool {
		// Apply every Direction to a ZonePosition and check if the other ZonePosition appears.
		for dir in Direction::list().iter() {
			if let Ok(zone_pos_dir) = self.pos_apply_dir(
				zone_pos_1,
				*dir
			) {
				if *zone_pos_2 == zone_pos_dir {
					return true;
				}
			}
		}

		false
	}
}