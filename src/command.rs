use std::collections::HashSet;

use dastan_base::{BlueprintOutput, Body, Equipment, Food, Furniture, Humanoid, Item, OrganicSlot, Profession, ToCreature, ToItem, Toggle, item::furniture::PartialConstruction};

use crate::{game::Game, map::ZonePosition, message_log::Origin};

pub trait Command<T: ToCreature> {
	fn execute(
		&self,
		game: &mut Game,
		creature: &mut T
	);
}

// This is not a Command because I need to own `food`. EatCommand would need to own it, but fn execute asks for &self.
fn creature_eat(
	game: &mut Game,
	creature: &mut Humanoid,
	food: Food,
) {
	game.message_log.add(
		format!("{} eats {}.", creature.name, food.name),
		Origin::Inventory
	);

	creature.eat(food);
}

pub struct PickItemUpFromMap {
	pub zone_pos: ZonePosition,
}

impl Command<Humanoid> for PickItemUpFromMap {
	fn execute(
		&self,
		game: &mut Game,
		creature: &mut Humanoid
	 ) {
		let item = game.map_mut()
			.zone_tile_mut(&self.zone_pos)
			.item_take()
			.unwrap()
		;

		game.message_log.add(
			format!("{} picks up {}.", creature.name, item.name()),
			Origin::Inventory
		);

		creature.inventory.pick_up(item).unwrap();
	}
}

pub struct EatFromMap {
	pub zone_pos: ZonePosition,
}

impl Command<Humanoid> for EatFromMap {
	fn execute(&self, game: &mut Game, creature: &mut Humanoid) {
		let food = game.map_mut()
			.zone_tile_mut(&self.zone_pos)
			.item_take()
			.unwrap()
			.into()
		;

		creature_eat(game, creature, food);
	}
}

pub struct DropFromInventory {
	// humanoid_zone_pos: ZonePosition,
	pub index: usize,
	pub drop_zone_pos: ZonePosition,
}

impl Command<Humanoid> for DropFromInventory {
	fn execute(
		&self,
		game: &mut Game,
		creature: &mut Humanoid
	 ) {
		// let humanoid: &mut Humanoid = game.map_mut()
		// 	.zone_tile_mut(&self.humanoid_zone_pos)
		// 	.creature_mut()
		// 	.as_mut()
		// 	.unwrap()
		// 	.humanoid_mut().unwrap()
		// ;
		let item = creature.inventory.remove(self.index);

		game.message_log.add(
			format!("{} drops {}.", creature.name, item.name()),
			Origin::Inventory
		);

		game.map_mut().zone_tile_mut(&self.drop_zone_pos).item_put(item);
	}
}

fn humanoid_equip(
	game: &mut Game,
	humanoid: &mut Humanoid,
	equipment: Equipment
) {
	game.message_log.add(
		format!("{} equips {}.", humanoid.name, equipment.name),
		Origin::Equipment
	);

	humanoid.equip(equipment);
}

pub struct EquipFromInventory {
	pub index: usize,
}

impl Command<Humanoid> for EquipFromInventory {
	fn execute(
		&self,
		game: &mut Game,
		creature: &mut Humanoid
	 ) {
		let equipment: Equipment = creature.inventory
			.remove(self.index)
			.into()
		;

		humanoid_equip(game, creature, equipment);
	 }
}

pub struct EquipFromMap {
	pub zone_pos: ZonePosition,
}

impl Command<Humanoid> for EquipFromMap {
	fn execute(&self, game: &mut Game, creature: &mut Humanoid) {
		let equipment = game.map_mut()
			.zone_tile_mut(&self.zone_pos)
			.item_take()
			.unwrap()
			.into()
		;

		humanoid_equip(game, creature, equipment);
	}
}

pub struct EatFromInventory {
	pub index: usize,
}

impl Command<Humanoid> for EatFromInventory {
	fn execute(
		&self,
		game: &mut Game,
		creature: &mut Humanoid
	) {
		let food: Food = creature.inventory.remove(self.index).into();

		creature_eat(game, creature, food);
	}
}

// pub struct PlantFromInventory {

// }

// impl PlantFromInventory {
// 	fn execute(
// 		&self,
// 		game: &mut Game,
// 		creature: &mut Humanoid
// 	) {
	// let item = game.tile_player_mut().item_take().unwrap();
	// game.message_log.add(
	// 	format!("{} plants {}, crushing any other item in that tile...",
	// 		player.name, item.name()
	// 	),
	// 	Origin::Inventory
	// );
	// game.tile_player_mut().item_put(item);
// 	}
// }

pub struct DequipFromEquipment {
	pub slot: OrganicSlot,
}

impl Command<Humanoid> for DequipFromEquipment {
	fn execute(&self, game: &mut Game, creature: &mut Humanoid) {
		let equipment = creature.dequip(self.slot).unwrap();

		game.message_log.add(
			format!("{} dequips {}.",
				creature.name, equipment.name
			),
			Origin::Equipment
		);

		creature.inventory.pick_up(equipment).unwrap();
	}
}

pub struct DropFromEquipment {
	// humanoid_zone_pos: ZonePosition,
	pub slot: OrganicSlot,
	pub drop_zone_pos: ZonePosition,
}

impl Command<Humanoid> for DropFromEquipment {
	fn execute(&self, game: &mut Game, creature: &mut Humanoid) {
		let equipment = creature.dequip(self.slot).unwrap();

		game.message_log.add(
			format!("{} dequips and drops {}.",
				creature.name, equipment.name
			),
			Origin::Equipment
		);

		game.map_mut()
			.zone_tile_mut(&self.drop_zone_pos)
			.item_put(equipment)
		;
	}
}

pub struct BuildBlueprint {
	pub profession: Profession,
	pub index: usize,
	pub ingredients: HashSet<usize>,
	pub zone_pos: ZonePosition
}

impl Command<Humanoid> for BuildBlueprint {
	fn execute(&self, game: &mut Game, creature: &mut Humanoid) {
		let blueprint = creature.recipe_book
			.blueprints_by_profession(&self.profession)[self.index]
			.clone()
		;

		let partial_construction = PartialConstruction::new(
			blueprint
		);

		creature.inventory.remove_multiple(&self.ingredients);

		game.map_mut()
			.zone_tile_mut(&self.zone_pos)
			.item_put(partial_construction)
		;
	}
}

pub struct FinishPartialConstruction {
	pub zone_pos: ZonePosition,
}

impl Command<Humanoid> for FinishPartialConstruction {
	fn execute(&self, game: &mut Game, _creature: &mut Humanoid) {
		let partial_construction = game.map_mut()
			.zone_tile_mut(&self.zone_pos)
			.item_take()
			.map(|item| match item {
				Item::Furniture(box Furniture::PartialConstruction(p)) => p,
				_ => panic!(),
			})
			.unwrap()
		;

		match partial_construction.blueprint.output {
			BlueprintOutput::Furniture(key) => {
				let furniture = game.gen_bundle.gen_furniture(&key);
				game.map_mut()
					.zone_tile_mut(&self.zone_pos)
					.item_put(furniture)
				;
			},
			BlueprintOutput::Terrain(new_terrain_type) => {
				game.map_mut().terrain_cover(&self.zone_pos, new_terrain_type);
			},
		};
	}
}

pub struct CraftFromRecipes {
	pub profession: Profession,
	pub index: usize,
	pub ingredients: HashSet<usize>,
}

impl Command<Humanoid> for CraftFromRecipes {
	fn execute(&self, game: &mut Game, creature: &mut Humanoid) {
		// let inventory = match in_workshop {
		// 	Some(workshop) => &mut workshop.input,
		// 	None => &mut player.inventory,
		// };
		let inventory = &mut creature.inventory;

		inventory.remove_multiple(&self.ingredients);

		let output_item_key = &creature.recipe_book
			.recipes_by_profession(&self.profession)[self.index].output
		;
		let item = game.gen_bundle.gen_item(&output_item_key);

		game.message_log.add(
			format!("{} crafts {}", creature.name, output_item_key),
			Origin::CraftRecipe
		);

		inventory.push(item);
	}
}

// Action::Forget(profession, i) => {
// 	player.recipe_book.recipe_remove(profession, i);

// 	Ok(GameAction::Redraw)
// },

pub struct PlayerTarget {
	pub target_zone_pos: ZonePosition,
}

impl<T: ToCreature> Command<T> for PlayerTarget {
	fn execute(&self, game: &mut Game, _creature: &mut T) {
		game.set_player_target(self.target_zone_pos);
	}
}

pub struct TogglePassage {
	pub zone_pos: ZonePosition,
}

impl<T: ToCreature> Command<T> for TogglePassage {
	fn execute(&self, game: &mut Game, creature: &mut T) {
		// let mut passage = game.map_mut()
		// 	.zone_tile_mut(&self.zone_pos)
		// 	.item_mut()
		// 	.as_mut()
		// 	.unwrap()
		// 	.into()
		// ;

		let mut passage = game.map_mut()
			.zone_tile_mut(&self.zone_pos)
			.item_take()
			.map(|item| match item {
				Item::Furniture(box Furniture::Passage(passage)) => passage,
				_ => panic!()
			})
			.unwrap()
		;

		passage.toggle();

		let status =
			if passage.blocks_passage {
				"locked"
			}
			else {
				"unlocked"
		};

		game.message_log.add(
			format!("{} {} {}.", creature.name(), status, passage.name),
			Origin::Furniture
		);

		game.map_mut()
			.zone_tile_mut(&self.zone_pos)
			.item_put(passage)
		;
	}
}