pub fn deploy_building(
	&mut self,
	top_left: &ZonePosition,
	construction_layout: &ConstructionLayout,
	rotate: bool,
	v_flip: bool,
	h_flip: bool,
	gen_bundle: &GeneratorBundle
) {
	// debug!("deploying building with orientation {:?}", orientation);

	let mut zone_pos = *top_left;

	let y_range = if flip {
		(0..construction_layout.height()).rev().collect()
	} else {
		(0..construction_layout.height()).collect()
	};

	let x_range = if rotate {
		(0..construction_layout.width()).rev().collect()
	} else {
		(0..construction_layout.width()).collect()
	};

	// let (i_range, j_range): (Vec<usize>, Vec<usize>) = match orientation {
	// 	Orientation::North => (
	// 		(0..construction_layout.height()).collect(),
	// 		(0..construction_layout.width()).collect()
	// 	),
	// 	Orientation::South => (
	// 		(0..construction_layout.height()).rev().collect(),
	// 		(0..construction_layout.width()).collect()
	// 	),
	// 	Orientation::East => (
	// 		(0..construction_layout.width()).collect(),
	// 		(0..construction_layout.height()).collect()
	// 	),
	// 	Orientation::West => (
	// 		(0..construction_layout.width()).rev().collect(),
	// 		(0..construction_layout.height()).rev().collect()
	// 	),
	// };

	for y in &y_range {
		for x in &x_range {
			let icon = match orientation {
				Orientation::North
				| Orientation::South => {
					construction_layout.matrix
						.get(*i)
						.unwrap()
						.get(*j)
						.unwrap()
				},
				Orientation::East
				| Orientation::West => {
					construction_layout.matrix
						.get(*j)
						.unwrap()
						.get(*i)
						.unwrap()
				}
			};

			let icon 

			// TODO I remove any Item that could be in that zone_pos so that I can place whatever is specified in construction_layout.
			self.zone_tile_mut(&zone_pos).item_take();

			match construction_layout.mapping.get(&icon).unwrap() {
				ConstructionType::Terrain(terrain) => {
					// let terrain = gen_bundle.gen_terrain(code);
					// let terrain = self.terrain(code);
					let terrain_type = gen_bundle.terrain_bundle.name_to_type(terrain);
					self.terrain_cover(&zone_pos, terrain_type);
				},
				ConstructionType::Item {item, terrain} => {
					let item = gen_bundle.gen_item(item);
					self.zone_tile_mut(&zone_pos).item_put(item);

					// let terrain = gen_bundle.gen_terrain(terrain_code);
					// let terrain = self.terrain(terrain_code);
					let terrain_type = gen_bundle.terrain_bundle.name_to_type(terrain);
					self.terrain_cover(&zone_pos, terrain_type);
				},
			};

			zone_pos = self.pos_apply_dir(
				&zone_pos,
				Direction::Right
			).unwrap();
		}

		zone_pos = self.pos_apply_dir(&zone_pos, Direction::Down).unwrap();
		zone_pos.pos.x = top_left.pos.x;
	}
}