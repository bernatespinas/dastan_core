use std::collections::HashSet;

use dastan_base::{LivingId, UuidIdentifier};

pub struct DebugLogger {
	livings: HashSet<LivingId>,
}

impl Default for DebugLogger {
	fn default() -> Self {
		DebugLogger {
			livings: HashSet::new(),
		}
	}
}

impl DebugLogger {
	pub fn start_tracking(&mut self, living_id: LivingId) {
		if self.livings.insert(living_id) {
			debug!("[DEBUG_PANEL] Logging {} from now on", living_id);
		} else {
			debug!("[DEBUG_PANEL] I was told to track {} but I was already doing that", living_id);
		}
	}

	/// Log a message if LivingId is being tracked.
	pub fn log_living(&self, living_id: &LivingId, message: &str) {
		if self.livings.contains(living_id) {
			debug!("[{}] {}", living_id.uuid(), message);
		}
	}
}