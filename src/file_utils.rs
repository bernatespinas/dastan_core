use std::path::Path;
use std::fmt::Display;

use crate::map::{Zone, ZoneId};

pub const UNIVERSES_DIR: &str = "../saves/universes";
pub const PLAYERS_DIR: &str = "../saves/players";

#[cfg(feature = "with_sdl2")]
pub const TTF_FONT_FILE: &str = "/home/bernat/Elieroz3.ttf";
#[cfg(feature = "with_sdl2")]
pub const TEXTURE_ATLAS_DIR: &str = "./resources/texture_atlas";
#[cfg(feature = "with_sdl2")]
pub fn texture_atlas_file_path(file: &str) -> String {
	format!("{}/{}", TEXTURE_ATLAS_DIR, file)
}

pub fn player_file(player_name: &str) -> String {
	format!("{}/{}", PLAYERS_DIR, player_name)
}

// Universe.

pub fn universe_dir(universe: &str) -> String {
	format!("{}/{}", UNIVERSES_DIR, universe)
}

pub fn universe_file(universe: &str) -> String {
	format!("{}/{}", universe_dir(universe), universe)
}

// Space.

pub fn space_dir(universe: &str) -> String {
	format!("{}/space", universe_dir(universe))
}

// Planets.

pub fn planets_dir(universe: &str) -> String {
	format!("{}/planets", universe_dir(universe))
}

pub fn planet_dir(universe: &str, planet: &str) -> String {
	format!("{}/{}", planets_dir(universe), planet)
}

// Spaceships.

pub fn spaceships_dir(universe: &str) -> String {
	format!("{}/spaceships", universe_dir(universe))
}

pub fn spaceship_dir(universe: &str, spaceship: &str) -> String {
	format!("{}/{}", spaceships_dir(universe), spaceship)
}

pub fn get_filenames_in_path(path: &str) -> Result<Vec<String>, String> {
	let save_files = std::fs::read_dir(Path::new(path))
		.map_err(|_| format!("Unable to read {}", path))?
	;

	Ok(save_files.filter_map(|entry| entry.ok().and_then(
			|e| e.path().file_name().and_then(
				|n| n.to_str().map(
					// OMG (Clippy)
					// |s| String::from(s)
					String::from
				)
			)
		)).collect::<Vec<String>>())
}

// Zones.

pub fn map_zone_path(map_dir: &str, zone_id: &ZoneId) -> String {
	format!("{}/zones/{}_{}", map_dir, zone_id.0, zone_id.1)
}

pub fn map_buffer_zone_path(map_dir: &str, zone_id: &ZoneId) -> String {
	format!("{}/zones/buffer/{}_{}", map_dir, zone_id.0, zone_id.1)
}

/// Read a compressed Zone file, decompress it, and deserialize it.
pub fn load_zone_path(
	map_dir: &str,
	zone_id: &ZoneId
) -> Result<Zone, String> {
	let zstd_file_path = map_zone_path(map_dir, zone_id);
	let decompressed = decompress_file(zstd_file_path)?;

	deserialize_bytes(
		decompressed.as_slice(),
		&format!("zone {:?}", zone_id)
	)
}

/// Save a compressed serialized Zone in the buffer dir of a Map.
pub fn save_zone_path_buffer(
	map_dir: &str,
	zone_id: &ZoneId,
	zone: &Zone
) -> Result<(), String> {
	// debug!("saving zone at {}", append_zone_to_map_dir(map_dir, zone_id));
	serialize_compress_and_write(
		zone,
		map_buffer_zone_path(map_dir, zone_id),
		&format!("zone {:?}", zone_id)
	)
}

/// Move all Zone files in a Map's buffer dir to its zones dir.
pub fn replace_buffer_zones(
	map_dir: &str
) -> Result<(), String> {
	let zones_dir = format!("{}/zones", map_dir);
	let buffer_zones_dir = format!("{}/buffer", &zones_dir);

	let buffer_zones_dir_files = get_filenames_in_path(&buffer_zones_dir)?;
	for zone_path in buffer_zones_dir_files {
		replace_file(
			&format!("{}/{}", buffer_zones_dir, zone_path),
			&format!("{}/{}", zones_dir, zone_path),
			"buffer zone"
		)?;
	}

	Ok(())
}

// Generic operation.

pub fn write_file<P, C>(path: P, contents: C, tag: &str) -> Result<(), String>
where
	P: AsRef<Path>,
	C: AsRef<[u8]>
{
	std::fs::write(path, contents).map_err(
		|_| format!("Error while writing {} file", tag)
	)
}

pub fn serialize<T>(value: &T, tag: &str) -> Result<String, String>
where
	T: serde::Serialize,
{
	ron::ser::to_string(value).map_err(|_| format!(
		"Error while serializing {}", tag
	))
}

pub fn serialize_and_write<T, P>(
	value: &T,
	path: P,
	tag: &str
) -> Result<(), String>
where
	T: serde::Serialize,
	P: AsRef<Path> + Display,
{
	// debug!("serializing and writing something at {}", path);

	let contents = serialize(value, tag)?;

	write_file(path, &contents, tag)
}

pub fn serialize_compress_and_write<T, P>(
	value: &T,
	path: P,
	tag: &str
) -> Result<(), String>
where
	T: serde::Serialize,
	P: AsRef<Path> + Display,
{
	let contents = serialize(value, tag)?;

	let zstd_contents = compress(contents);

	write_file(path, &zstd_contents, tag)
}

fn compress(source: String) -> Vec<u8> {
	let mut destination: Vec<u8> = Vec::new();

	zstd::stream::copy_encode(
		source.as_bytes(),
		&mut destination,
		1
	).unwrap();

	destination
}

fn decompress_file<P>(path: P) -> Result<Vec<u8>, String>
where
	P: AsRef<Path> + Display,
{
	let source = std::fs::File::open(&path)
		.map_err(|_| format!("Error while opening file {}", path))?
	;
	let mut destination: Vec<u8> = Vec::new();

	zstd::stream::copy_decode(&source, &mut destination)
		.map_err(|_| format!("Error while decompressing (zstd) {}", path))?
	;

	Ok(destination)
}

pub fn read<P>(path: P, tag: &str) -> Result<String, String>
where
	P: AsRef<Path> + Display,
{
	// debug!("reading something at {}", path);

	std::fs::read_to_string(path).map_err(|_| format!(
		"Error while reading {} file", tag
	))
}

pub fn deserialize<'a, T>(s: &'a str, tag: &str) -> Result<T, String>
where
	T: serde::Deserialize<'a>,
{
	ron::de::from_str(s).map_err(|_| format!(
		"Error while deserializing {}", tag
	))
}

fn deserialize_bytes<'a, T>(s: &'a [u8], tag: &str) -> Result<T, String>
where
	T: serde::Deserialize<'a>,
{
	ron::de::from_bytes(s).map_err(|_| format!(
		"Error while deserializing {}", tag
	))
}

// pub fn read_and_deserialize<'a, T, P>(path: P, tag: &str) -> Result<T, String>
// where
// 	T: serde::Deserialize<'a>,
// 	P: AsRef<Path>,
// {
// 	let s = read(path, tag)?;

// 	// TODO "s" doesn't live long enough... 'a...
// 	deserialize(&s, tag)
// }

pub fn remove_file<P>(path: P, tag: &str) -> Result<(), String>
where
	P: AsRef<Path>,
{
	std::fs::remove_file(path).map_err(|_| format!(
		"Error while removing {} file", tag
	))
}

pub fn remove_dir_all<P>(path: P, tag: &str) -> Result<(), String>
where
	P: AsRef<Path>,
{
	std::fs::remove_dir_all(path).map_err(|_| format!(
		"Error while removing {} directory", tag
	))
}

pub fn replace_file(src: &str, dest: &str, tag: &str) -> Result<(), String> {
	std::fs::rename(src, dest).map_err(|_| format!(
		"Error while renaming src {} file to {} ({})", src, dest, tag
	))
}

pub fn create_dir_all<P>(path: P, tag: &str) -> Result<(), String>
where
	P: AsRef<Path>,
{
	std::fs::create_dir_all(path).map_err(|_| format!(
		"Error while creating {} dir and subdirs", tag
	))
}