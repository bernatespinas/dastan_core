pub struct EmailAddress<'a>(&'a str);

impl<'a> EmailAddress<'a> {
	fn new(address: &'a str) -> Result<EmailAdress<'a>, ()> {
		if address.matches("[a-zA-Z0-9-_]+@[a-z]+\.[a-z]+") {
			Ok(Email(address))
		}
		else {
			Err(())
		}
	}
}

pub struct Email<'a> {
	from: EmailAddress<'a>,
	// to: EmailAddress<'a>,
	subject: &'a str,
	content: &'a str,
	// TODO: Clone Time?
	date: Time,
}

impl<'a> Email<'a> {
	pub fn new(from: EmailAddress<'a>, subject: &'a str, content: &'a str, date: &Time) -> Email<'a> {
		Email {
			from,
			subject,
			content,
			date,
		}
	}
}

pub struct Internet {
	phone_numbers: HashSet<u16> or HashMap<u16>,
	// emails: HashSet<String> or HashMap<String>,
	emails: HashMap<EmailAddress, Vec<Email>>,
	// file_systems: HashMap<
}

impl Internet {
	pub fn add_email_address(email_address: EmailAddress) -> Result<String, String> {
		if self.emails.contains(&email_address) {
			Err("This email address already exists!".to_string())
		}
		else {
			self.emails.insert(email_address, Vec::new());
			Ok("Email address successfully registered!".to_string())
		}
	}
	
	pub fn send_email(email: Email) {
		self.emails.get(email.from)
	}
}