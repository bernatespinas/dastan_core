use std::collections::HashSet;
use std::fmt;

use dastan_base::Color;

use crate::game::Location;
use crate::time::Time;

// TODO Rename to Category?
#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone, Serialize, Deserialize)]
pub enum Origin {
	AttackFromPlayer,
	AttackToPlayer,
	Inventory,
	Equipment,
	Quest,
	Profession,
	Furniture,
	Build,
	CraftRecipe,
}

impl fmt::Display for Origin {
	fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
		write!(f, "{:?}", self)
	}
}

#[derive(Serialize, Deserialize)]
pub struct Message {
	pub text: String,
	pub color: Color,
	pub origin: Origin,
	// location: Location,
	// time: Time,
}

impl Message {
	pub fn new(text: String, origin: Origin) -> Message {
		let color = match origin {
			Origin::Inventory			=> Color::Yellow,
			Origin::Equipment			=> Color::Orange,
			Origin::AttackFromPlayer	=> Color::LBlue,
			Origin::AttackToPlayer		=> Color::Red,
			_							=> Color::DGrey,
		};
		Message {
			text,
			color,
			origin,
		}
	}
}

#[derive(Serialize, Deserialize)]
pub struct MessageLog {
	origins: HashSet<Origin>,
	locations: HashSet<Location>,
	times: HashSet<Time>,
	pub messages: Vec<Message>,
	pub new_messages: bool,
}

impl MessageLog {
	pub fn new() -> MessageLog {
		MessageLog {
			origins: HashSet::new(),
			locations: HashSet::new(),
			times: HashSet::new(),
			messages: Vec::new(),
			new_messages: false,
		}
	}

	pub fn add(&mut self, text: String, origin: Origin) {
		self.add_msg(Message::new(text, origin));
	}

	pub fn add_msg(&mut self, message: Message) {
		if ! self.origins.contains(&message.origin) {
			self.origins.insert(message.origin.clone());
		}
		// if ! self.locations.contains(message.location) {
		// 	self.locations.push(message.location);
		// }
		// if ! self.times.contains(message.time) {
		// 	self.times.push(message.time);
		// }

		// TODO Wrap messages that have to be displayed.
		self.messages.push(message);
		self.new_messages = true;
	}

	// fn origin_is_present(&self, origin: &Origin) -> bool {
	// 	for orgn in &self.origins {
	// 		if orgn == origin {
	// 			return true;
	// 		}
	// 	}
	// 	false
	// }
}