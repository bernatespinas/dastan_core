use std::collections::HashMap;

use crate::{ai::Ai, map::{Position, ZoneId, ZonePosition, pathfinding::Pathfinding}};

use super::Game;

pub trait AiSystem {
	fn process(
		game: &mut Game,
		pathfinding: &mut Pathfinding,
		redraw_map: bool
	);
}

pub struct CreatureAiSystem { }

impl AiSystem for CreatureAiSystem {
	fn process(
		game: &mut Game,
		pathfinding: &mut Pathfinding,
		redraw_map: bool
	) {
		// The AIs and their Positions in each currently loaded Zone.
		let creature_ais = game.map_mut().creature_ais_take();

		// The new collection that will replace the old one.
		let mut new_creature_ais: HashMap<ZoneId, HashMap<Position, Ai>> =
			HashMap::with_capacity(creature_ais.capacity())
		;

		for (zone_id, ai_positions) in creature_ais {
			for (ai_pos, mut ai) in ai_positions {
				let ai_src_zone_pos = ZonePosition {
					zone_id,
					pos: ai_pos
				};

				// Extract that creature from the Map to be able to modify both of them.
				let mut creature = match game.map_mut()
					.zone_mut(&zone_id)
					.tile_mut(&ai_pos)
					.creature_take()
				{
					Some(creature_with_ai)	=> creature_with_ai,
					// If there isn't any Creature there, skip that. It might have died or get killed or something. TODO Log it.
					None => {
						debug!("Missing Creature on AI ZonePosition {:?}", ai_src_zone_pos);
						continue;
					},
				};

				// Make the creature take a turn, which returns its new position in the Map.
				let ai_dst_zone_pos = match ai.take_turn(
					&mut creature,
					ai_src_zone_pos,
					game,
					pathfinding
				) {
					Ok(zp) => zp,
					Err(msg) => {
						debug!("{}", msg);
						continue;
					}
				};

				// If new_ais doesn't have an entry for this AIs' zone_id (because it moved to a different Zone), create it.
				if !new_creature_ais.contains_key(&ai_dst_zone_pos.zone_id) {
					new_creature_ais.insert(ai_dst_zone_pos.zone_id, HashMap::new());
				}

				// If this creature is the player's target, update its position. TODO I'm checking this for EVERY Creature...
				if game.is_player_target(&ai_src_zone_pos) {
					game.set_player_target(ai_dst_zone_pos);
					// target_health.redraw = true;
					// target_source.redraw = true;
				}

				// Place the creature in the grid again.
				game.map_mut()
					.zone_tile_mut(&ai_dst_zone_pos)
					.creature_put(creature)
				;

				// If the map isn't going to be completely redrawn when this turn ends, draw the current AI's Creature if it's visible and clear its previous Tile if it's visible.
				if !redraw_map {
					game.tiles_pending_redraw.insert(ai_dst_zone_pos);
					game.tiles_pending_redraw.insert(ai_src_zone_pos);
					// if map_pad.is_visible(&ai_dst_zone_pos, self.map()) {
					// 	map_pad.draw_tile(
					// 		self.map(),
					// 		&ai_dst_zone_pos,
					// 		&self.gen_bundle.terrain_bundle,
					// 		context
					// 	);
					// }

					// if map_pad.is_visible(&ai_src_zone_pos, self.map()) {
					// 	map_pad.draw_tile(
					// 		self.map(),
					// 		&ai_src_zone_pos,
					// 		&self.gen_bundle.terrain_bundle,
					// 		context
					// 	);
					// }
				}

				// Insert the AI and its new position into the soon-to-be self.ais HashMap.
				new_creature_ais
					.get_mut(&ai_dst_zone_pos.zone_id)
					.unwrap()
					.insert(ai_dst_zone_pos.pos, ai)
				;
			}
		}

		game.map_mut().creature_ais_set(new_creature_ais);
	}
}

pub struct PlantAiSystem {

}

pub struct ComputerAiSystem {
	
}