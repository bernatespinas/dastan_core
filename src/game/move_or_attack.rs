use direction_simple::Direction;

use dastan_base::ToCreature;

use crate::game::{Game, GameAction};
use crate::map::{Map, ZonePosition};
use crate::message_log::Origin;

pub enum Action {
	Move,
	Attack,
	Pass,
}

pub fn walk(zone_pos: &ZonePosition, map: &Map) -> Action {
	let tile = map.zone_tile(zone_pos);

	if tile.creature_there() {
		Action::Attack
	} else if tile.can_walk() {
		Action::Move
	} else {
		Action::Pass
	}
}

pub fn swim(zone_pos: &ZonePosition, map: &Map) -> Action {
	let tile = map.zone_tile(zone_pos);

	if tile.creature_there() {
		Action::Attack
	} else if tile.can_swim() {
		Action::Move
	} else {
		Action::Pass
	}
}

pub type MovementFn = fn(zone_pos: &ZonePosition, map: &Map) -> Action;

// This action returns a Result because moving might trigger a reloding of Zones, which can fail.
pub fn action<T: ToCreature>(
	game: &mut Game,
	player: &mut T,
	direction: Direction
) -> Result<GameAction, String> {
	let initial_player_zone_pos = game.player_zone_pos();

	let zone_pos =
		if let Ok(zone_dir) = game.map().pos_apply_dir(
			&initial_player_zone_pos,
			direction
		) {
			zone_dir
		} else {
			return Ok(GameAction::NotTakeTurn);
		}
	;

	match game.get_movement_fn()(&zone_pos, game.map()) {
		Action::Move => {
			// If the player entered a new Zone, reload some new zones in that direction.
			if !initial_player_zone_pos.same_zone(&zone_pos) {
				game.map_mut().update_loaded_zones(&zone_pos.zone_id)?;
			}

			game.tiles_pending_redraw.insert(initial_player_zone_pos);

			game.player_pos_update(&zone_pos);

			Ok(GameAction::Move(zone_pos, direction))
		},

		Action::Attack => {
			let creature = game.map_mut()
				.zone_tile_mut(&zone_pos)
				.creature_mut()
				.as_mut()
				.unwrap()
			;

			let message = player.attack(creature);

			// Check if the Creature is alive after the attack. >:D
			if creature.is_alive() {
				// If the player doesn't have a target, set creature as the new one.
				if !game.has_player_target() {
					game.set_player_target(zone_pos);
				}

				game.message_log.add(message, Origin::AttackFromPlayer);
			} else {
				die(game, message, zone_pos);
			}

			Ok(GameAction::TakeTurn)
		}
		
		Action::Pass => Ok(GameAction::NotTakeTurn),
	}
}

fn die(
	game: &mut Game,
	message: String,
	zone_pos: ZonePosition
) {
	// TODO Tell Game the player's target has died, so it knows what to do or something.
	// if game.is_player_target(&zone_pos) {
	// 	game.clear_player_target();

	// 	// Manually clear the target's panels when it dies.
	// 	// They aren't cleared automatically. 
	// 	target_health.win.delete_content();
	// 	target_source.win.delete_content();
	// }

	// The Creature dies and becomes a Corpse, TODO: crushing any Item that happened to be in the same Tile.
	game.map_mut().creature_to_corpse(&zone_pos);

	// Redraw the target's tile.
	game.tiles_pending_redraw.insert(zone_pos);

	game.message_log.add(message, Origin::AttackFromPlayer);
}