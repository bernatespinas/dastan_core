use std::collections::HashMap;
use std::convert::TryFrom;

use hierarchical_pathfinding::prelude::ManhattanNeighborhood;
use hierarchical_pathfinding::internals::AbstractPath;

use dastan_base::{Color, Toggle, Identifier};
use dastan_base::creature::{Creature, ToCreature};
use dastan_base::item::Item;
use dastan_base::item::furniture::Furniture;

use crate::game::Game;
use crate::map::{Position, ZonePosition};
use crate::map::pathfinding::Pathfinding;

pub type CreatureAi = Ai;

trait TakeTurn: Into<AiTask> {
	fn take_turn(
		&mut self,
		ai: &mut Ai,
		creature: &mut Creature,
		game: &mut Game,
		zone_pos: ZonePosition
	) -> Result<(), String>;

	/// Find a goal ZonePosition the Ai should try to find a path to. If no goal can be set, say so (None). TODO
	fn set_goal(
		&mut self,
		ai: &mut Ai,
		game: &mut Game,
		src_zone_pos: ZonePosition
	) -> Option<ZonePosition>;
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum AiPriority {
	Survival,
	Combat,
	Work,
	Social,
}

impl AiPriority {
	fn priority_list() -> [AiPriority; 4] {
		[
			AiPriority::Survival,
			AiPriority::Combat,
			AiPriority::Work,
			AiPriority::Social
		]
	}
}

impl From<Vec<(AiPriority, AiTask)>> for Ai {
    fn from(ai_info: Vec<(AiPriority, AiTask)>) -> Self {
		let mut ai_tasks = HashMap::new();
		
		for (ai_priority, ai_task) in ai_info {
			// TODO Use try_insert and then unwrap.
			if ai_tasks.insert(ai_priority, ai_task).is_some() {
				panic!("Repeated {:?} while creating Ai from ai_info", ai_priority);
			}
		}

		Ai {
			ai_tasks,
			goal: None,
			path: None,
			move_here_next_turn: None,
		}
    }
}

impl From<&Vec<(AiPriority, AiTask)>> for Ai {
    fn from(ai_info: &Vec<(AiPriority, AiTask)>) -> Self {
		let mut ai_tasks = HashMap::new();
		
		for (ai_priority, ai_task) in ai_info {
			// TODO Use try_insert and then unwrap.
			if ai_tasks.insert(*ai_priority, ai_task.clone()).is_some() {
				panic!("Repeated {:?} while creating Ai from 'ai_info'", ai_priority);
			}
		}

		Ai {
			ai_tasks,
			goal: None,
			path: None,
			move_here_next_turn: None,
		}
    }
}

enum CommitMove {
	Move(ZonePosition),
	WaitCreature(ZonePosition),
	ReloadCostMatrixAndPath,
	TogglePassage(ZonePosition),
	GoalReached,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct Ai {
	ai_tasks: HashMap<AiPriority, AiTask>,

	goal: Option<ZonePosition>,

	#[serde(skip)]
	path: Option<AbstractPath<ManhattanNeighborhood>>,

	// Sometimes you need the Ai to move to a particular ZonePosition. For example, after unlocking a door, its ZonePosition gets removed from the path (it has been processed), so we store it here to force the Ai to move there next turn (instead of skipping it).
	move_here_next_turn: Option<ZonePosition>,
	
	// ai_state: AiState,
}

impl Ai {
	fn highest_priority_ai_task(&mut self) -> Option<(AiPriority, AiTask)> {
		for ai_priority in &AiPriority::priority_list() {
			if let Some(ai_task) = self.ai_tasks.remove(&ai_priority) {
				return Some((*ai_priority, ai_task));
			}
		}

		None
	}

	fn insert_ai_task(&mut self, ai_priority: AiPriority, ai_task: AiTask) {
		self.ai_tasks.insert(ai_priority, ai_task);
	}

	/// If there's a Creature at the destination ZonePosition, return the current one. Maybe a Creature moved during the same turn and after waiting for a few turns they will be gone. TODO
	fn commit_move_or_change_path(
		&mut self,
		game: &Game,
		src_zone_pos: ZonePosition,
		dst_zone_pos: ZonePosition,
		pathfinding: &mut Pathfinding
	) -> CommitMove {
		let dst_tile = game.map().zone_tile(&dst_zone_pos);

		// IF DST IS GOAL, ASK AI_TASK WHAT TO DO OR SOMETHING (ATTACK A CREATURE INSTEAD OF WAITING, ETC)

		if dst_tile.creature_there() {
			// If there's a Creature in that Tile, stay still.
			CommitMove::WaitCreature(src_zone_pos)
		// } else if dst_tile.blocks_passage_terrain_item() {
		} else if dst_tile.unlockable_passage() {
			CommitMove::TogglePassage(dst_zone_pos)
		} else if pathfinding.cache_needs_update(&dst_zone_pos, game.map()) {
			// If there's an "unexpected" Item or Terrain that blocks_passage, find another path. TODO
			// if let Some(item) = game.map().zone_tile(&dst_zone_pos).item() {
			// 	if let Item::Furniture(box Furniture::Passage(_)) = item {
			// 		CommitMove::TogglePassage(dst_zone_pos)
			// 	} else {
			// 		CommitMove::ReloadCostMatrixAndPath
			// 	}
			// } else {
				CommitMove::ReloadCostMatrixAndPath
			// }
		} else {
			CommitMove::Move(dst_zone_pos)
		}
	}

	pub fn take_turn(
		&mut self,
		creature: &mut Creature,
		zone_pos: ZonePosition,
		game: &mut Game,
		pathfinding: &mut Pathfinding
	) -> Result<ZonePosition, String> {
		// If we told the Ai to move to a specific ZonePosition, return it and skip any other logic.
		if let Some(zp) = self.move_here_next_turn.take() {
			return Ok(zp);
		}

		// Get the AiTask to process (the one with the highest priority).
		let (ai_priority, mut ai_task) = match self.highest_priority_ai_task() {
			Some((ai_priority, ai_task)) => (ai_priority, ai_task),
			None => return Err("No AiTask to process".to_string()),
		};

		ai_task.take_turn(self, creature, game, zone_pos)?;
		let goal = ai_task.set_goal(self, game, zone_pos);

		match goal {
			Some(g) => {
				if self.goal != goal {
					self.path = pathfinding.find_path(
						&zone_pos,
						&g,
						game.map()
					);
					self.goal = goal;
				}
			},
			None => {},
		};

		let commit_move = match &mut self.path {
			Some(path) => match path.next() {
				Some(next) => {
					let y = u16::try_from(next.1).unwrap();
					let x = u16::try_from(next.0).unwrap();

					let dst_zone_pos = ZonePosition::new(
						zone_pos.zone_id, Position::new(y, x)
					);

					self.commit_move_or_change_path(
						game,
						zone_pos,
						dst_zone_pos,
						pathfinding
					)
				},
				// TODO
				None => CommitMove::Move(zone_pos),
			},
			// TODO
			None => CommitMove::Move(zone_pos),
		};

		match commit_move {
			CommitMove::Move(dst) => {
				self.insert_ai_task(ai_priority, ai_task);

				Ok(dst)
			},
			CommitMove::WaitCreature(src) => {
				game.logging.log_living(
					&creature.id().0.unwrap(),
					"[PATHFINDING] Waiting for Creature to get out of my way"
				);
				self.insert_ai_task(ai_priority, ai_task);

				Ok(src)
			},
			CommitMove::TogglePassage(zp) => {
				match game.map_mut().zone_tile_mut(&zp).item_mut() {
					Some(Item::Furniture(box Furniture::Passage(passage))) => {
						passage.toggle();
						game.tiles_pending_redraw.insert(zp);
					},
					_ => panic!("expected passage to toggle"),
				};

				self.move_here_next_turn = Some(zp);
				self.insert_ai_task(ai_priority, ai_task);

				Ok(zone_pos)
			},
			CommitMove::ReloadCostMatrixAndPath => {
				debug!("[PATHFINDING] Reloading cost matrix and path");
				// let zone = game.map().zone(&zone_pos.zone_id);
				// pathfinding.add_zone_path_cache(&zone_pos.zone_id, zone);
				pathfinding.invalidate_zone(&zone_pos.zone_id);
				self.path = None;

				self.insert_ai_task(ai_priority, ai_task);

				Ok(zone_pos)
			},
			CommitMove::GoalReached => {
				game.logging.log_living(
					&creature.id().0.unwrap(),
					"[PATHFINDING] Goal reached"
				);
				self.insert_ai_task(ai_priority, ai_task);
				
				Ok(zone_pos)
			},
		}
	}
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum AiTask {
	RunAwayFromCreature(RunAwayFromCreature),
	ChaseCreature(ChaseCreature),
	Patrol(Patrol),
	CrazyChameleon(CrazyChameleon),
}

impl TakeTurn for AiTask {
	fn take_turn(
		&mut self,
		ai: &mut Ai,
		creature: &mut Creature,
		game: &mut Game,
		zone_pos: ZonePosition
	) -> Result<(), String> {
		match self {
			AiTask::RunAwayFromCreature(ai_task) => ai_task.take_turn(ai, creature, game, zone_pos),
			AiTask::ChaseCreature(ai_task) => ai_task.take_turn(ai, creature, game, zone_pos),
			AiTask::Patrol(ai_task)
				=> ai_task.take_turn(ai, creature, game, zone_pos),
			AiTask::CrazyChameleon(ai_task)
				=> ai_task.take_turn(ai, creature, game, zone_pos),
		}
	}

	fn set_goal(
		&mut self,
		ai: &mut Ai,
		game: &mut Game,
		src_zone_pos: ZonePosition
	) -> Option<ZonePosition> {
		match self {
			AiTask::RunAwayFromCreature(ai_task)
				=> ai_task.set_goal(ai, game, src_zone_pos),
			AiTask::ChaseCreature(ai_task)
				=> ai_task.set_goal(ai, game, src_zone_pos),
			AiTask::Patrol(ai_task)
				=> ai_task.set_goal(ai, game, src_zone_pos),
			AiTask::CrazyChameleon(ai_task)
				=> ai_task.set_goal(ai, game, src_zone_pos),
		}
	}
}

#[derive(Debug, Copy, Clone, Hash, Serialize, Deserialize)]
pub struct RunAwayFromCreature {
	turns: u16,
}

impl From<RunAwayFromCreature> for AiTask {
	fn from(ai: RunAwayFromCreature) -> AiTask {
		AiTask::RunAwayFromCreature(ai)
	}
}

impl TakeTurn for RunAwayFromCreature {
	fn take_turn(
		&mut self,
		_ai: &mut Ai,
		_creature: &mut Creature,
		_game: &mut Game,
		_zone_pos: ZonePosition
	) -> Result<(), String> {
		Ok(())
	}

	fn set_goal(
		&mut self,
		_ai: &mut Ai,
		_game: &mut Game,
		src_zone_pos: ZonePosition
	) -> Option<ZonePosition> {
		// TODO
		Some(src_zone_pos)
	}
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ChaseCreature {
	turns: u16,

	last_know_zone_pos: Option<ZonePosition>,
}

impl From<ChaseCreature> for AiTask {
	fn from(ai: ChaseCreature) -> AiTask {
		AiTask::ChaseCreature(ai)
	}
}

impl Default for ChaseCreature {
	fn default() -> Self {
		ChaseCreature {
			turns: 0,
			last_know_zone_pos: None,
		}
	}
}

impl ChaseCreature {
	pub fn new(chased_creature_zone_pos: ZonePosition) -> ChaseCreature {
		ChaseCreature {
			last_know_zone_pos: Some(chased_creature_zone_pos),
			..ChaseCreature::default()
		}
	}

	// fn update_last_known_zone_pos(&mut self, game: &Game) {
	// 	// TODO Do something simple, such as stopping to chase the Creature if it's too far away (and define when that's the case).
	// }
}

// TODO Hardcoded player position.
impl TakeTurn for ChaseCreature {
	fn take_turn(
		&mut self,
		_ai: &mut Ai,
		creature: &mut Creature,
		game: &mut Game,
		zone_pos: ZonePosition
	) -> Result<(), String> {
		// if ai.path.is_some() {
			self.last_know_zone_pos = Some(game.player_zone_pos());
		// }

		if game.map().are_adjacent(&game.player_zone_pos, &zone_pos) {
			debug!("{} attacks player!!", creature.name());
		}

		Ok(())
	}

	fn set_goal(
		&mut self,
		_ai: &mut Ai,
		_game: &mut Game,
		_src_zone_pos: ZonePosition
	) -> Option<ZonePosition> {
		self.last_know_zone_pos
	}
}

#[derive(Debug, Copy, Clone, Hash, Serialize, Deserialize)]
pub struct Patrol {

}

impl From<Patrol> for AiTask {
	fn from(ai: Patrol) -> AiTask {
		AiTask::Patrol(ai)
	}
}

impl TakeTurn for Patrol {
	fn take_turn(
		&mut self,
		_ai: &mut Ai,
		_creature: &mut Creature,
		_game: &mut Game,
		_zone_pos: ZonePosition
	) -> Result<(), String> {
		Ok(())
	}

	fn set_goal(
		&mut self,
		_ai: &mut Ai,
		_game: &mut Game,
		src_zone_pos: ZonePosition
	) -> Option<ZonePosition> {
		// TODO
		Some(src_zone_pos)
	}
}

#[derive(Debug, Copy, Clone, Hash, Serialize, Deserialize)]
pub struct CrazyChameleon {
	crazy_color: Color,
}

impl Default for CrazyChameleon {
	fn default() -> Self {
		CrazyChameleon {
			crazy_color: Color::LBlue,
		}
	}
}

impl TakeTurn for CrazyChameleon {
    fn take_turn(
		&mut self,
		_ai: &mut Ai,
		creature: &mut Creature,
		_game: &mut Game,
		_zone_pos: ZonePosition
	) -> Result<(), String> {
		match creature {
			Creature::Humanoid(box ref mut humanoid) => {
				let temp_color = humanoid.color;
				humanoid.color = self.crazy_color;
				self.crazy_color = temp_color;
			},
			Creature::Animal(box ref mut animal) => {
				let temp_color = animal.color;
				animal.color = self.crazy_color;
				self.crazy_color = temp_color;
			},
			Creature::Spaceship(..) => {},
		};

		Ok(())
	}

	fn set_goal(
		&mut self,
		_ai: &mut Ai,
		_game: &mut Game,
		src_zone_pos: ZonePosition
	) -> Option<ZonePosition> {
		Some(src_zone_pos)
	}
}

impl From<CrazyChameleon> for AiTask {
    fn from(crazy_chameleon: CrazyChameleon) -> Self {
        AiTask::CrazyChameleon(crazy_chameleon)
    }
}