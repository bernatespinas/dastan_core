use nrustes::Color;

use dastan_base::Creature;

use crate::Game;
use crate::map::ZonePosition;

pub type CreatureAi = Box<dyn Ai>;
pub type AiType = Box<dyn Ai>;

#[typetag::serde]
pub trait Ai: Send + Sync {
	fn take_turn(
		&mut self,
		zone_pos: ZonePosition,
		game: &mut Game
	) -> ZonePosition;
}

#[derive(Clone, Serialize, Deserialize)]
pub struct BasicEnemy {
}

impl BasicEnemy {
	pub fn new() -> BasicEnemy {
		BasicEnemy {

		}
	}
}

#[derive(Clone, Serialize, Deserialize)]
pub struct CrazyChameleon {
	pub crazy_color: Color,
}

impl CrazyChameleon {
	pub fn new() -> CrazyChameleon {
		CrazyChameleon {
			crazy_color: Color::LBlue,
		}
	}
}

fn take_creature_or_log_missing<'a>(
	game: &'a mut Game,
	zone_pos: &ZonePosition
) -> Option<&'a mut Creature> {
	game
		.map_mut()
		.zone_tile_mut(zone_pos)
		.creature_mut()
		.as_mut()
		.or_else(|| {
			debug!("Missing Creature on AI ZonePosition {:?}", zone_pos);
			None
		})
}

#[typetag::serde]
impl Ai for CrazyChameleon {
	fn take_turn(
		&mut self,
		zone_pos: ZonePosition,
		game: &mut Game
	) -> ZonePosition {
		let creature = take_creature_or_log_missing(game, &zone_pos);

		match creature {
			Some(Creature::Humanoid(humanoid)) => {
				let temp_color = humanoid.color;
				humanoid.color = self.crazy_color;
				self.crazy_color = temp_color;
			},
			Some(Creature::Animal(animal)) => {
				let temp_color = animal.color;
				animal.color = self.crazy_color;
				self.crazy_color = temp_color;
			},
			Some(Creature::Spaceship(..)) => {},
			None => {},
		};

		zone_pos
	}
}

#[typetag::serde]
impl Ai for BasicEnemy {
	fn take_turn(&mut self, zone_pos: ZonePosition, game: &mut Game) -> ZonePosition {
		zone_pos
	}
}